#!/bin/sh
#Get path from standard input
if [ -z "$1" ]
	then 
		echo "invalid input file!"; 
		continue; 
else 
	fastafilepath=$1;
	currentpath=$PWD;
	workpath="$currentpath";
	cd $workpath;
	foldName="random";
	mkdir $foldName;
	chmod -R 777 $foldName;
	cd $foldName;
	cp "$fastafilepath/input.fna" "random.fna";
	cp "$fastafilepath/input.ptt" "random.ptt";
	cp "$fastafilepath/input.faa" "random.faa";
	cd $currentpath;
	perl ./operonScripts/prepareFilesFinal.pl "$foldName";
fi;
