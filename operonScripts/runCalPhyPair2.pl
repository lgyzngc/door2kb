#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = $ARGV[0]; #"ppc64linux";
   my $dir = $ARGV[1]; #"\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $evalue = $ARGV[2]; #1e-20;
   my $genomeNum = $ARGV[3]; #675;
   my $NC_name = $ARGV[4];          
   my $NC_pair = $ARGV[5]; #$NC_name .".adjPair";
   my $NC_hom = $ARGV[6]; #$NC_name . ".ffn.hom";
   my $seqout=$ARGV[7]; #"/scratch/phd/$NC_name.phyDist";
   my $scriptpath = $ARGV[8];
   ####################################################################
   
     
     my $out1 = "/scratch/phd/$NC_name.extract";
     my $cmdfile="$dir\/$NC_name.phy.sh";
     printjobfile();

sub printjobfile{
     
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE "if [ ! -e $dir\/$NC_hom ]; then scp $dir\/$NC_hom /scratch/phd/; fi \n";
     print JOBFILE "perl $scriptpath\/extract_phylo_wEcutoff_ffn.pl \/scratch\/phd\/$NC_hom $evalue $genomeNum \> $out1\n";
     print JOBFILE "perl $scriptpath\/cal-phylo-dist-for-gene-pair.pl $out1 $dir\/$NC_pair \>$seqout\n";
     
     print JOBFILE "scp $seqout $dir/\n";
     print JOBFILE "rm -rf \/scratch\/phd\/$NC_name*\n";

     close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
