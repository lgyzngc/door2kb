#!/usr/local/bin/perl -w

my $inputfile = $ARGV[0];
my @NC;
my @genome;

open (ANN, "<$inputfile") or die "Can not open $inputfile....exit\n";

while(<ANN>)
{
   chomp;
   my $string = $_;

   if (!($string eq "") &&($string =~ /NC_00/)) {
       my @arr1 = split(/\s/,$string);
       my @arr = split(/\./,$arr1[1]);
       push (@NC,$arr[0]);
       print "$arr[0] ";
   }elsif (!($string eq "")) {
       my @arr = split (/-/,$string);
       #rint "$arr[-1]\n";
       my $line = $arr[-1];
       $line =~ s/\s//g;
       my @arr1 = split(/\./,$line);
       print "$arr1[2]\n";
       
   }
}

close(ANN);

