#!/usr/local/bin/perl -w

## cat ptt and rnt file, then run sort file

#print commands to run large number of files
   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $pttdir = "\/gpfs0\/home\/staff\/phd\/pttMay08";
   my $program = "sort_ptt_rnt_file.pl";


   my $inFile = $ARGV[0];          # This is the file name without ptt or rnt
	####################################################################
   my $ptt = $inFile . ".ptt";
   my $rnt = $inFile . ".rnt.mod";
   my $out1 = $inFile . ".ptt_rnt";
   my $out2 = $inFile . ".ptt_rnt.sorted";

     my $cmdfile="$dir\/$inFile.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE "scp $pttdir\/$ptt /scratch/phd/$out1 \n";
     print JOBFILE "if  [ -e $pttdir/$rnt ] ; then cat $pttdir/$rnt >>/scratch/phd/$out1; fi\n";
     
     
     print JOBFILE "perl $dir\/$program /scratch/phd/$out1 \> /scratch/phd/$out2\n";
     print JOBFILE "scp /scratch/phd/$out2 $pttdir\/\n";
     print JOBFILE "rm -rf  /scratch/phd/$out1\n";
     print JOBFILE "rm -rf /scratch/phd/$out2\n"; 
     close JOBFILE;

     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.5);


