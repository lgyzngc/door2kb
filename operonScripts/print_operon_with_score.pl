#!/usr/local/bin/perl -w

# This program is used to print operon from svm output
# the file has atleast 3 columns.  The gene name in column 1,2  has to be in 
# sequential order lengthwise. Also need ptt file for print single gene
# if the score in column 3 is less than or equal threshold, will count as operon, other wise, boundary pair
# WILL ASSUME THAT THE ORDER OF THE PAIR IS CORRECT!!!!


my @directon;
my @score;
my @curr_pair;
my @new_pair;
my $maxScore = 6;
my $minScore = 1;

my $inFile = $ARGV[0]; # This is the pair file with scores
my $threshold = $ARGV[1];
my $pttfile = $ARGV[2]; ##ptt file name

my %geneSingle; ##gene and it position in the genome
my %tab;
my @gene;
my $genePos = 3; ## collumn with gene name in the ptt file
if ( @ARGV <1) {
    print "usage: pair-file threshold ptt \ WILL TAKE EQUAL SIGN\n";
    die;
} 
if (!defined $threshold) {
    $threshold = 0; ## AUTOMATICALLY ASSIGN THRESHOLD WHEN NEEDED
}
   ####################################################################

processptt();

open (ANN, "<$inFile") or die "Can not open $inFile ... exit\n";

 #my $str = <ANN>; ##first line

while (<ANN>) {
    my $str = $_; 
    chomp ($str); 
    my @arr = split (/\s/, $str);
    	if ($arr[2] < $minScore) {
	    $arr[2] = $minScore;
	}elsif ($arr[2] > $maxScore) {
	    $arr[2] = $maxScore;
	}
    if (!defined ($directon[0]) && ($arr[2] <= $threshold)) {
	@directon =  ($arr[0], $arr[1]);
	@curr_pair = ($arr[0], $arr[1]);

	@score =( $maxScore,$arr[2]);
	$geneSingle{$arr[0]} = 0;
	$geneSingle{$arr[1]} = 0;

    }elsif (!defined ($directon[0])) {

    }elsif ($arr[2]<= $threshold) {
	@new_pair = ($arr[0], $arr[1]);
	if (!defined $new_pair[0] ||  ($new_pair[0] eq "") 
	    || !defined $new_pair[1] ||  ($new_pair[1] eq "") ) {
	    if ($directon[0] ne ""){
		print "$tab{$directon[0]}\t";
		my $lscore = 0;
		for ( my $i = 0; $i <@directon; ++$i) {
		    print "$directon[$i] ";
		    $lscore += $score[$i];
		}
		$lscore = $lscore/(scalar@directon);
		print "\t@score\n";
	    }
	    @directon =();
	    @score = ();
	}else {
	    if ($curr_pair[1] eq $new_pair[0]) {
		push (@directon, $new_pair[1]);
		@curr_pair = @new_pair;
		$geneSingle{$new_pair[1]} = 0;
		if ($arr[2] < $minScore) {
		    $arr[2] = $minScore;
		}
		push(@score, $arr[2]);
	    }else {
		if ($directon[0] ne "") {
		    print "$tab{$directon[0]}\t";
		    my $lscore = 0;
		    for ( my $i = 0; $i <@directon; ++$i) {
			print "$directon[$i] ";
			$lscore += $score[$i];
		    }
		    $lscore = $lscore/(scalar@directon);
		    print "\t@score\n";
		    
		}
		@directon =@new_pair;
		@score= ($maxScore,$arr[2]);
		@curr_pair = @new_pair;
		$geneSingle{$new_pair[0]} = 0;
		$geneSingle{$new_pair[1]} = 0;
	    }
	}
	
    }	 
}
close(ANN);
if (defined($directon[0]) &&($directon[0] ne "")){
    print "$tab{$directon[0]}\t";
    my $lscore = 0;		    
    for (my $i= 0; $i <@directon; ++$i) {
	print "$directon[$i] ";
	$lscore += $score[$i];
    }
    $lscore = $lscore/(scalar@directon);
		    print "\t@score\n";
}
for (my $i = 0; $i < @gene; ++$i) {
    if ($geneSingle{$gene[$i]} > 0) {
	print "$geneSingle{$gene[$i]}\t$gene[$i]\t$maxScore\n";
    }
}
sub processptt{
    my $ind = 1;
    open (PTT, "<$pttfile") or die "Can not open $pttfile ... exit\n";
    my $lc = 0;
    while ($lc < 3) {
	my $ltr = <PTT>;
	++$lc;
    }
    while (<PTT>) {
	my @lar = split(/\t/,$_);
	push(@gene, $lar[$genePos]);
	$geneSingle{$lar[$genePos]} = $ind;
	$tab{$lar[$genePos]} = $ind;
	++$ind;
    }
    close (PTT);
}
