#!/usr/local/bin/perl -w

## SORT THE FILE numerically

my $inputfile = $ARGV[0]; 
my $out = $ARGV[1];
my $col = 0; 
my @arr;

if ((@ARGV < 1)) {
    print "Usage: sort numerically \n";
   die "Not enough parameters: inputfile, col_to_sort....exit\n";
}
open (OUT, ">$out");
open (ANN, "<$inputfile") or die "Can not open $inputfile....exit\n";

while(<ANN>)
{
    if (defined ($_)) {
	chomp;  
        if ($_ ne "") {
	    $_ =~ s/^\s//;
	    $_ =~ s/\Z\s//;
	    $_ =~ s/\s+/ /g;
	    push (@arr, $_);
	}
    }
}
close(ANN);

my @sort_arr =();
my $non_dig = 0;
OUTER: for (my $i = 0; $i < @arr; ++$i) {
    #print "i $i start sort_arr @sort_arr\n";
    if ($i == 0) {
	push (@sort_arr, $arr[$i]);
	#print "push first ele @sort_arr\n";
    }else {
	my @c_arr = split (/\s/,$arr[$i]);
	my $c_item = $c_arr[$col];
	#print "c_item $c_item\n";
	if (($c_item eq "") || (!($c_item =~ /\d/))){
	   push (@sort_arr, $arr[$i]);
	   ++$non_dig;
	   next OUTER;
	}
	$c_item =~ s/\s//g; ##MAKE SURE C-ITEM IS NOT EMPTY
	my $stop = 0;
	my $s_ind = 0;
	for (my $j =0; ($j < @sort_arr) && ($stop == 0); ++$j) {
	    my @in_arr = split (/\s/,$sort_arr[$j]);
	    #print "in_arr $in_arr[$col]\n";
	    if ((!defined $in_arr[$col]) || ( $in_arr[$col] eq "") || (!($in_arr[$col] =~ /\d/))){
		++$stop;
		$s_ind = $j-1;
		#print "stop because $in_arr[$col]\n";
	    }else {
		$in_arr[$col] =~ s/\s//g;
		if ($in_arr[$col] > $c_item) {
		    ++$stop;
		    $s_ind = $j-1;
		}
	    }
	}
	#print "s_ind $s_ind\n";
	if ($s_ind < 0) {
	    #print "unshift\n";
	    unshift(@sort_arr,$arr[$i]);
	    #print "arr-i $arr[$i] sort-arr-i @sort_arr\n";
	}elsif ($stop == 0) {
	    #print "push at the end\n";
	    push (@sort_arr, $arr[$i]);
	}else {
	    my $leng = $s_ind +1;
	    my $n_leng = @sort_arr - $leng;
	    my @temp = @sort_arr;
	    my @temp1 = splice (@sort_arr,0,$leng);
	    my @temp2 = splice (@temp,$leng,$n_leng);
	    #print "temp1 @temp1\ntemp2 @temp2\n";
	    push (@temp1,$arr[$i]);
	    foreach (@temp2) {
		push(@temp1,$_);
	    }
	    @sort_arr = @temp1;
	    #print "@sort_arr\n";
	}
    }
}

for (my $i = 0; $i < @sort_arr; ++$i) {
    print OUT (($i+1),"\t");
    my @c_arr = split (/\s/,$sort_arr[$i]);
	my $c_item = $c_arr[$col];
	if (($c_item eq "") || (!($c_item =~ /\d/))){
	    print OUT "$sort_arr[$i]\n";   
	}else {
	    $c_item =~ s/\s//g;
	    if ($c_item < 0) {
		my $rank = $i+1;
		$rank = -$rank;
		print OUT "$sort_arr[$i]\n";
	    }else {
		my $rank = @sort_arr - $i - $non_dig;
		for (my $k = 1; $k < @c_arr; ++$k) {
		    print OUT "$c_arr[$k] ";
		}
		print OUT "\n";
		#print "$sort_arr[$i]\n";
	    }
	}
}
close(OUT);
