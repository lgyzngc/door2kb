#!/usr/bin/perl -w 
use strict;
my $i=0;
open (FILE,"../list")||die "$_";
while (my $line=<FILE>){
     chomp($line);
     my $inFile=$line;
     $i++;
     open JOBFILE,">job/job.$inFile.sh" || die "$_";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=glinx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "perl ../prepareFilesFinal.pl $inFile\n";
     close JOBFILE;
     system("chmod 755 job/job.$inFile.sh");

     system("qsub job/job.$inFile.sh");
     #sleep(3);
     exit if ($i==50);

}
close FILE;
