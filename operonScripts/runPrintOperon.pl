#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = $ARGV[0]; #"ppc64linux";
   my $path = $ARGV[1]; #"\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $scriptpath = $ARGV[2]; #"operons"; 
   my $inFile = $ARGV[3];          # This is the file contain NC number
   my $scorefile = $ARGV[4]; 
   my $phytail = $ARGV[5]; #".phyDist";
   my $lengtail = $ARGV[6]; #".lengRat";
   my $igtail = $ARGV[7]; #".IG";
   my $mot1tail = $ARGV[8];#".motif";
   my $mot2tail = $ARGV[9];#".motif";
   my $neitail = $ARGV[10];#".neiDist";
   my $neitail2 = $ARGV[11];#".neiNext";
   my $val = $ARGV[12]; #3; ## threshold for the cutoff score
   my $ptt = $ARGV[13];
   my $oprout = $ARGV[14];
   ####################################################################
  printjobfile();


sub printjobfile{
     
     my $seqout="/scratch/phd/$oprout";
     my $out1 = "/scratch/phd/$inFile.op";

     my $cmdfile="$path/$inFile\/$inFile.score.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE "perl $scriptpath/assignScore.pl $inFile $path/$inFile/$scorefile $path/$inFile $phytail $lengtail $igtail $mot1tail $mot2tail $neitail $neitail2\n";
     print JOBFILE " perl $scriptpath\/print_operon_from_svm.pl $path/$inFile/$scorefile $val $path/$inFile/$ptt $out1\n";

     print JOBFILE " perl $scriptpath\/sort_operon.pl $out1 $seqout\n";

     print JOBFILE "scp $seqout $path\/$inFile/$oprout\n";
     print JOBFILE "rm -rf \/scratch\/phd\/$inFile*\n";

     close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
