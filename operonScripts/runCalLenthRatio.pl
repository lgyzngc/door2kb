#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = "ppc64linux";
   my $database = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $pttdir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08\/pttMay08";
   my $lengRatdir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08\/lengRatio";
my $pairdir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08\/genePairs";
   my $name = $ARGV[0];          #sequence file
   ####################################################################
  printjobfile($name);


sub printjobfile{
     my $NC_name = shift();
     my $NC_pair = $NC_name .".adjPair";
     my $lnr = "$NC_name" . ".ptt_rnt.sorted";
     my $seqout="/scratch/phd/$NC_name".".lengRat";

     my $cmdfile="$database\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE "perl $database\/cal-length-ratio-between-gene-pairs-v2.pl $pttdir/$lnr $pairdir\/$NC_pair \>$seqout\n";
     print JOBFILE "scp $seqout $lengRatdir\/\n";
     print JOBFILE "rm -rf $seqout\n";
    # print JOBFILE "rm -rf /scratch/phd/$NC_name\n"; 
    # print JOBFILE "rm -rf /scratch/phd/$NC_pair\n";
    close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.5);
}
