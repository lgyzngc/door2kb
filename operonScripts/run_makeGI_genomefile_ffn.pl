#!/usr/local/bin/perl -w

## cat ptt and rnt file, then run sort file

#print commands to run large number of files
   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/pttMay08";
   my $pttdir = "\/gpfs0\/home\/staff\/phd\/pttMay08";
   my $program = "makeGI_genomefile_ffn.pl";
   my $genome = "genomes.675.txt";
   my $nc = "NC_list.1225.txt";

   my $ptt = $ARGV[0];          # This is the ptt_rnt.sort file
	####################################################################
     my $out = $ptt .".short";
     my $cmdfile="$dir\/$ptt.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE "scp $pttdir\/$ptt /scratch/phd/ \n";
    
     print JOBFILE "perl $dir\/$program /scratch/phd/$ptt $dir/$genome $dir/$nc /scratch/phd/$out\n";
     print JOBFILE "scp /scratch/phd/$out $pttdir\/\n";
     print JOBFILE "rm -rf  /scratch/phd/$out\n";
     close JOBFILE;

     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.5);


