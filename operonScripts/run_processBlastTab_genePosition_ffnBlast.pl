#!/usr/local/bin/perl -w

## cat ptt and rnt file, then run sort file

#print commands to run large number of files
   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $ptt = "allGenePositionMay08";
   my $program = "processBlastTab_genePosition_ffnBlast.pl";
   my $blastdir ="/gpfs0/home/staff/phd/blastRes051408";

   my $inFile = $ARGV[0];          # This is the file name without ptt or rnt
	####################################################################
   my $out = $inFile . ".frn.hom";
   my $fileName = $inFile . ".frn.blastMicrobe051408.gz";

     my $cmdfile="$dir\/$inFile.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     #print JOBFILE "rm -rf /scratch/phd/NC_*.sorted\n";
     #print JOBFILE "rm -rf /scratch/phd/microbeDB*\n";
     #print JOBFILE "rm -rf /scratch/phd/NC_*.txt\n";
     print JOBFILE "if  [ ! -e /scratch/phd/$ptt ] ; then scp $dir/$ptt /scratch/phd/; fi\n";
     
     
     print JOBFILE "perl $dir\/$program $blastdir/$inFile/$fileName /scratch/phd/$ptt /scratch/phd/$out\n";
     print JOBFILE "scp /scratch/phd/$out $dir\/\n";
     print JOBFILE "rm -rf  /scratch/phd/$out\n";
      #print JOBFILE "rm -rf  /scratch/phd/$inFile\n";
     close JOBFILE;

     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.5);


