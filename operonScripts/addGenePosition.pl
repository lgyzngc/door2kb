#!/usr/local/bin/perl -w
# this is used to process blast table without header (-m 8)

my $file = $ARGV[0]; ##blast table
my $exfile = $ARGV[1]; ##table of genePID, NC # and genome #
#my $newfile = $ARGV[2]; ##output

my %tab;

open (ANN1,"<$exfile") or die "can not open $exfile\n";

while (<ANN1>){
   chomp;
   my @arr = split (/\s/,$_);
   my $line ="";
   for (my $j = 1; $j < @arr; ++$j){
       $line = $line . "," .$arr[$j];
   }
   $tab{$arr[0]} = $line;


}
close (ANN1);
open (ANN, "<$file") or die "can not open $file\n";

while (<ANN>){
    chomp;
    my $line = $_;
    my @arr = split (/\s/,$line);
    for (my $i = 0; $i < @arr; ++$i) {
	my @lar = split (/\,/,$arr[$i]);
	my $eval = "0.0";
	if ($i > 0) {
	    $eval = $lar[-1];
	}
	my $curhit = $lar[0];
	$curhit =~ s/[(,)]//g;
	$eval =~ s/[(,)]//g;

	if (defined($tab{$curhit}) &&($tab{$curhit} ne "")) {
	    $arr[$i] = "(" .$curhit  .$tab{$curhit} ."," . $eval .")";
        }
    }
    print_qgi(@arr);
}
close(ANN);
#close(OUT);
sub print_qgi{
    my @lar = @_;
	for (my $i =0; $i < @lar; ++$i) {
	   print "$lar[$i]\t";
	}
        print "\n";   
}
