#!/usr/bin/perl
# build phylogenetic profile for a genome by extracting information from another file
# see NC_000117.hom for the format 
my $phy_file = $ARGV[0];
my $evalue = $ARGV[1];
my $numGenome = $ARGV[2];
my $out = $ARGV[3];

my @genomes;
my @genes;
my %cur_genome_phy;
my %gene_phy;

if (@ARGV <3) {
    print "Usage : phylo_file, evalue num_genome_\n";
    die;
}
if (!defined $evalue){
    $evalue = 0.0001;
}

###################
##PROCESS PHYLO FILE
open (OUT, ">$out");
if ($phy_file =~ /\.gz/){
    open (INFILE, "gunzip -c $phy_file |" ); ##OPEN FROM A ZIP FILE
}elsif (-e "$phy_file") {
    open INFILE, "$phy_file";
}else {
    die ("Cannot open phy_file  $phy_file\n");
}
while (<INFILE>){
    my $str = $_;
    chomp($str);
    my @curphy;
    for (my $i = 0; $i < ($numGenome+1); ++$i) {
        $curphy[$i] = 0;

    }
    my @l_arr = split (/\s/,$str); ##tab delimited file
    for (my $i = 0; $i < @l_arr; ++$i) {
	my $ln = $l_arr[$i];
	$ln =~ s/[\(\)]//g;
	my @llarr = split (/\,/,$ln);
	if ($i == 0) {
	    $curphy[$i] = $llarr[2];
	    #$curphy[$i] = $ln . ",0.0";
	}else {
	    if (checkEvalue($llarr[-1]) == 1){
                if ($curphy[$llarr[3]] eq "0") {
		    $curphy[$llarr[3]] =  1;
		   # $curphy[$llarr[3]] =  $ln;
		} #else {
		    
		  #  $curphy[$llarr[3]] = $curphy[$llarr[3]] ."_" . $ln; ##$llarr[0]; ### position of genome in the string is 4
		#}
	    }
	}

    }   
    print OUT "@curphy\n";
}
close (INFILE);
close (OUT);
#########################
########################
sub checkEvalue {
    my $l_str = shift;
    if ($l_str =~ /^e/){
	$l_str = "1" .$l_str;
    }
    if ($l_str <= $evalue) {
	return 1;
    }else {
	return 0;
    }
}
