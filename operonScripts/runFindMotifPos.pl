#!/usr/local/bin/perl -w
#print commands to run large number of files
   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $subdir = "genomeMotifPos";
   my $pttdir = "pttMay08";
   my $fnadir = "fnaMay08";
   my $geneIGdir = "genomeIG";
my $mot = "upstreamMotif";
   my $name = $ARGV[0];          #NC name

   my $leng = 300;
   ####################################################################
  printjobfile($name);

sub printjobfile{
     my $NC_name = shift();
     my $fna = $NC_name . ".fna";
     my $ptt = $NC_name . ".ptt_rnt.sorted";
     my $IG = $NC_name . ".IG";
     my $out0 = $NC_name . ".prom300";
     my $out1 = $NC_name .".MofPos";
     my $out2 = $NC_name . ".downstream";
     my $out3 = $NC_name . ".prom";
     my $sdir = "\/scratch\/phd";
     my $cmdfile="$dir\/$NC_name.sh";

     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";
     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "cd $sdir\n";

     print JOBFILE "scp $dir\/$fnadir\/$fna /scratch/phd/ \n";
     print JOBFILE "scp $dir\/$geneIGdir\/$IG $sdir\/\n";
     print JOBFILE "scp $dir\/$pttdir\/$ptt $sdir\/\n";
     print JOBFILE "scp $dir\/$mot $sdir\/\n"; 

     print JOBFILE "perl $dir\/cutUPstreamGenome.pl $sdir\/$ptt $sdir\/$fna $leng \>$sdir\/$out0\n"; ##cut upstream of whole genome
     print JOBFILE "perl $dir\/print_downstreamGI.pl $sdir\/$IG \> $sdir\/$out2\n"; ## print the downstream gene in the gene pair
     print JOBFILE "perl $dir\/findMotifPosition.pl $sdir\/$out0 $sdir\/$mot \>$sdir\/$out3\n"; ## find the motif frequency
     print JOBFILE "perl $dir\/printSelectMotif.pl $sdir\/$out2 $sdir\/$out3 \> $sdir/$out1\n"; ## print the promoter of the downstream gene
    

     print JOBFILE "scp $sdir\/$out1 $dir\/$subdir\/\n";
     print JOBFILE "rm -rf $sdir/$NC_name.*\n";
    close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
