#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = "ppc64linux";
   my $database = "\/gpfs0\/home\/staff\/phd\/operonPreMay08\/directons";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08\/genePairs";
my $path = "\/gpfs0\/home\/staff\/phd\/operonPreMay08\/";
   my $name = $ARGV[0];          #sequence file
   ####################################################################
  printjobfile($name);


sub printjobfile{
     my $NC_name = shift();
     my $infile = $NC_name . ".directon";
     my $lnr = "\/scratch\/phd\/$NC_name" .".directon";
     my $seqout="/scratch/phd/$NC_name.adjPair";
     my $cmdfile="$database\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";
     if(-e $seqout){
        system("rm $seqout");
     }
     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "scp $database\/$infile /scratch/phd/ \n";
     print JOBFILE "perl $path\/print_pair_in_operon.pl $lnr \>$seqout\n";
     print JOBFILE "scp $seqout $dir\/\n";
     print JOBFILE "rm -rf $seqout\n";
     print JOBFILE "rm -rf $lnr\n"; 
    close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
