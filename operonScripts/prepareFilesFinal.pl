#!/usr/bin/env perl -w
use FindBin qw($Bin);

#this script will calculate features for the operon prediction
# it will need a faa file, ptt file and sequence file and a database of available microbe genomes for comparison
# it will do:
#1. blast search of the target genome against the microdatabase
#2. process the blast file
#3. print the directon from ptt file
#4. print adjacent pair from the directon
#5. print next pair from directon
#6. calculate IG between adj pair
#7. calculate genelength ratio between adj pair
#8. calculate the motif frequency in the promoter region from fna file
#9. calculate the phylo distance
#10 calculate the neighbor distance

my $inFile = $ARGV[0];          # This is the file contain NC number

my $microbedbpath= "$Bin/../microbe110109_faaDB";
my $scriptpath ="$Bin";
my $path ="$Bin/..";

#my $microbedbpath= "/md3000/staff/maqin/operonPRED/microbe110109_faaDB";
#my $scriptpath ="/md3000/staff/maqin/operonPRED/operonScripts";
#my $path ="/md3000/staff/maqin/operonPRED";

#blast
my $microbedb="microbe110109_faa";
#my $blastpath = "/md3000/tools/i386/blast";
my $blastpath = "/cloud/www/html/DOOR/tools/blast-2.2.25-ia32";
my $linx = "glinux";
my $blast = "$scriptpath/runBlastppc_faa_e-2.pl";
my $blastout = $inFile .".faa.blast";
my $blastoutgz = $blastout .".gz";
my $processBlast = "$scriptpath/processBlastTab_faa.pl";
my $ncHom = $inFile .".faa.blast.hom";

#prepare gene ptt database
my $genomeList ="genomeList110109";
my $ncList = "NCList110109";
my $allgene ="allGene110109_ptt";
my $makeGI = "$scriptpath/addGI_genomefile_ffn.pl";
my $nallgene = $allgene .".$inFile";

#input file
my $outpath = $path . "/$inFile";
my $ptt = "$inFile" .".ptt";
my $faa = "$inFile" .".faa";
my $fna = "$inFile" .".fna";

#directon
my $printdirecton = "$scriptpath/print_directon_from_ptt_gi.pl";
my $ncdirecton = $inFile .".directon";

#adj pair &next pair ig, length ratio
my $printAdjPair = "$scriptpath/print_pair_in_operon.pl";
my $ncAdjPair = $inFile . ".adjPair";
my $ncNextPair = $inFile . ".nextPair";

#my $printNextPair = "$scriptpath/print_IG_between_pair.pl";
my $printNextPair = "$scriptpath/print_nextGene_in_operon.pl";
#my $runIG = "$scriptpath/print_IG_between_pair.pl";
my $runIG = "$scriptpath/print_IG_between_pair_with_gi.pl";
my $ncig = $inFile . ".ig";
my $runRatio = "$scriptpath/cal-length-ratio-between-gene-pairs-v2.pl";
my $ncRatio = $inFile .".rat";

#motif
my $motif = "$scriptpath/runCalMotif.pl";
my $mot = "upstreamMotif";
my $promLength = 100;
my $ncmotif = $inFile .".motif";

#phylo
my $phylo = "$scriptpath/runCalPhyPair3.pl";
my $phyeval = 1e-20;
my $genomeNum = 971;
my $ncphy = $inFile . ".phy";
my $ncphynext = $inFile . ".phynext";

#neighbor
my $neieval = 1e-20;
my $ncNum = 1822;
my $ncnei = $inFile . ".nei";
my $ncneinext = $inFile . ".neinext";

#score
my $score = "$scriptpath/runPrintOperon.pl";
my $ncscore = $inFile .".score";
my $scorethreshold = 2.9;
my $ncopr = $inFile .".opr";

my $scratchdir="$path/output/$inFile";
mkdir $scratchdir or die "Cann't mkdir $scratchdir: $!" unless -e $scratchdir; # Added by Chuan Zhou
#my $scratchdir = "/scratch/phd/$inFile/";
if (-e "$scratchdir"){
    system "rm -rf $scratchdir/*\n";
}else {
    system "mkdir -p $scratchdir\n";
}

#system ("cd $scratchdir\n");

runBlast();
runAddGeneToList();
runDirecton();
runAdjpair();
runMotif();
runProcessBlast();
runPhylo();
runScore();

system "rm $scratchdir/*\n";
system "rmdir $scratchdir\n";

sub runScore {
print "runScore begnin...";
    my $sdir = "$path/$inFile";
    system "perl $scriptpath/assignScore.pl $inFile $sdir/$ncscore $path/$inFile $ncphy $ncRatio $ncig $ncmotif $ncmotif $ncnei $ncneinext\n";
     system" perl $scriptpath\/print_operon_from_svm.pl $path/$inFile/$ncscore $scorethreshold $sdir/$ptt $scratchdir/$inFile.opr1\n";

     system " perl $scriptpath\/sort_operon.pl $scratchdir/$inFile.opr1 $sdir/$ncopr\n";

}

sub runPhylo {
print "runPhylo begin...";

    my $out1 = "$inFile.hom.phy.ext";
    my $out2 = "$inFile.hom.nei.ext";
    system "perl $scriptpath\/extract_phylo_wEcutoff_ffn.pl $path/$inFile/$ncHom $phyeval $genomeNum  $scratchdir/$out1\n";
     system "perl $scriptpath\/cal-phylo-dist-for-gene-pair.pl $scratchdir/$out1 $path/$inFile\/$ncAdjPair $path/$inFile/$ncphy\n";
     system "perl $scriptpath\/cal-phylo-dist-for-gene-pair.pl $scratchdir/$out1 $path/$inFile/$ncNextPair $path/$inFile/$ncphynext\n";

     system "perl $scriptpath\/extract_phylo_wEcutoff_ffn_printGI.pl $path/$inFile/$ncHom $neieval $ncNum  $scratchdir/$out2\n";
     system "perl $scriptpath\/calculatePairwiseHomologAdjacent_ffn.pl $scratchdir/$out2 $path/$inFile/$ncAdjPair $path/$inFile/$ncnei\n";
     system "perl $scriptpath\/calculatePairwiseHomologAdjacent_ffn.pl $scratchdir/$out2 $path/$inFile/$ncNextPair $path/$inFile/$ncneinext\n";
}

sub runMotif {
print "runMotif begin...";
    my $sdir = "$path/$inFile";
    system "perl $scriptpath\/cutUPstreamGenome.pl $sdir/$ptt $sdir\/$fna $promLength \>$scratchdir\/$inFile.prom\n"; ##cut upstream of whole genome
    system "perl $scriptpath\/print_downstreamGI.pl $sdir/$ncig \> $scratchdir\/$inFile.downstream\n"; ## print the downstream gene in the gene pair
    system "perl $scriptpath\/calculateMotifFrequency.pl $scratchdir\/$inFile.prom $scriptpath/$mot \>$scratchdir\/$inFile.prom.freq\n"; ## find the motif frequency
    system"perl $scriptpath\/printSelectMotif.pl $scratchdir\/$inFile.downstream $scratchdir\/$inFile.prom.freq \> $scratchdir/$ncmotif\n"; ## print the promoter of the downstream gene
    system "scp $scratchdir/$ncmotif $sdir/\n";
}

sub runProcessBlast {
print "runProcessBlast begin...";
   system "perl $processBlast $path/$inFile/$blastoutgz $path/$inFile/$nallgene $path/$inFile/$ncHom\n";
}

sub runAdjpair {
print "runAdjpair begin...";
    if (-e "$path/$inFile/$ncdirecton") {
	system "perl $printAdjPair $path/$inFile/$ncdirecton $path/$inFile/$ncAdjPair\n";
	system "perl $printNextPair $path/$inFile/$ncdirecton $path/$inFile/$ncNextPair\n";
	system "perl $runIG $path/$inFile/$ptt $path/$inFile/$ncAdjPair $path/$inFile/$ncig\n";
	system "perl $runRatio $path/$inFile/$ptt $path/$inFile/$ncAdjPair $path/$inFile/$ncRatio\n";
	
    }
}


sub runDirecton {
print "runDirecton begin...";
    system "perl $printdirecton $path/$inFile/$ptt $path/$inFile/$ncdirecton\n";
}

sub runAddGeneToList {
    my $ngenomeList = $genomeList . ".$inFile";
    my $nncList = $ncList .".$inFile";
    
    system "perl $makeGI $path/$inFile/$ptt $scriptpath/$genomeList $scriptpath/$ncList $path/$inFile/$nallgene $path/$inFile/$ngenomeList $path/$inFile/$nncList\n";
    system "cat $scriptpath/$allgene >> $path/$inFile/$nallgene \n"; 

}

sub runBlast{
#print "runBlast begin...";
    my $neval = 1e-2;
    my $b =5000;
    system "$blastpath/bin/blastall -p blastp -i $path/$inFile/$faa -d $microbedbpath/$microbedb -o $scratchdir/$blastout -I T -e $neval -m 8 -b $b \n";
    system "gzip $scratchdir/$blastout \n";
    system "cp $scratchdir/$blastoutgz $path/$inFile/\n";
    #system "cp $scratchdir/$blastoutgz $path/$inFile/\n";

    #print "$blastpath/bin/blastall -p blastp -i $path/$inFile/$faa -d $microbedbpath/$microbedb -o $scratchdir/$blastout -I T -e $neval -m 8 -b $b \n";
    #print "gzip $scratchdir/$blastout \n";
    #print "scp $scratchdir/$blastoutgz $path/$inFile/\n";
}
