#!/usr/local/bin/perl -w

#process error file, see error2_tmp for format

   my $name = $ARGV[0];          #name of error output file 
   my $out = $name . ".out";
   my $errStr = "BioseqFindFunc: couldn\'t uncache";
   ####################################################################
   open (ANN, "<$name") or die "Can not open $name....exit";
   open (OUT, ">$out") or die "can not open $out....exit";
   while (<ANN>){
      chomp;
      my $seqName = $_ ;
      if ($seqName !~ m/$errStr/) {
	  print OUT "$seqName\n";
      }
  }
close(ANN);
close(OUT);
system "mv $out $name\n";
