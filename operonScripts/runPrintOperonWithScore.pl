#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $subdir = "operonsWscore";
   my $val = 2.7; ## threshold for the cutoff
   my $name = $ARGV[0];
   ####################################################################
  printjobfile($name);


sub printjobfile{
     my $NC = shift();
     my $NC_name = "$dir\/genePairNextScore\/$NC" .".scoreN";
     my $ptt = "$dir\/pttMay08\/$NC" .".ptt_rnt.sorted";
     my $seqout="/scratch/phd/$NC.oprN";
     my $out1 = "/scratch/phd/$NC.op";

     my $cmdfile="$dir\/$NC.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE " perl $dir\/print_operon_with_score.pl $NC_name $val $ptt \> $out1\n";

     print JOBFILE " perl $dir\/sort_operon.pl $out1 \> $seqout\n";

     print JOBFILE "scp $seqout $dir\/$subdir\/\n";
     print JOBFILE "rm -rf \/scratch\/phd\/$NC_name*\n";

     close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
