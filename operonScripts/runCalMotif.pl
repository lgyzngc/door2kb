#!/usr/local/bin/perl -w
#print commands to run large number of files
   my $linx = $ARGV[0] ;
   my $dir = $ARGV[1];
   my $mot = $ARGV[2] ;
   my $name = $ARGV[3];          #NC name
   my $scriptpath = $ARGV[4] ;
   my $leng = $ARGV[5] ;
   my $out1 = $ARGV[6];
   ####################################################################
  printjobfile($name);

sub printjobfile{
     my $NC_name = shift();
     my $fna = $NC_name . ".fna";
     my $ptt = $NC_name . ".ptt";
     my $IG = $NC_name . ".ig";
     my $out0 = $NC_name . ".prom300";
     my $out2 = $NC_name . ".downstream";
     my $out3 = $NC_name . ".prom";
     my $sdir = "\/scratch\/phd";
     my $cmdfile="$dir\/$NC_name.motif.sh";

     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";
     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "cd $sdir\n";

     print JOBFILE "scp $dir\/$fna /scratch/phd/ \n";
     print JOBFILE "scp $dir\/$IG $sdir\/\n";
     print JOBFILE "scp $dir/$ptt $sdir\/\n";

     print JOBFILE "perl $scriptpath\/cutUPstreamGenome.pl $sdir\/$ptt $sdir\/$fna $leng \>$sdir\/$out0\n"; ##cut upstream of whole genome
     print JOBFILE "perl $scriptpath\/print_downstreamGI.pl $sdir\/$IG \> $sdir\/$out2\n"; ## print the downstream gene in the gene pair
     print JOBFILE "perl $scriptpath\/calculateMotifFrequency.pl $sdir\/$out0 $mot \>$sdir\/$out3\n"; ## find the motif frequency
     print JOBFILE "perl $scriptpath\/printSelectMotif.pl $sdir\/$out2 $sdir\/$out3 \> $sdir/$out1\n"; ## print the promoter of the downstream gene
    

     print JOBFILE "scp $sdir\/$out1 $dir\/\n";
     print JOBFILE "rm -rf $sdir/$NC_name.*\n";
     close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
