#!/usr/local/bin/perl -w
## use to sort file by the first column (gene location
## This is taytored to the ptt file and .rnt files
## Will need the list with the name of the files
## the input is a mix of .ptt and .rnt file.  will sort it according to the location

## SORT THE FILE numerically according to the first column

my $ptt = $ARGV[0]; ## name of the file to sort
my $rnt = $ARGV[1];
my $out = $ARGV[2]; 
my %a;
my $str;
my $str1;
my $str2;
my $str3;
if ((!$ARGV[0])) {
   die "Not enough parameters: ptt rnt out ....exit\n";
}
processFile($ptt);
if (-e "$rnt") {
    processFile($rnt);
}
printFile();

sub processFile {
    my $lfile = shift();
    open (ANN, "<$lfile") or die "Can not open $lfile....exit\n";
    if ($lfile =~ /.ptt/){
	$str1 = <ANN>;
	$str2 = <ANN>;
	$str3 = <ANN>;
    }else {
	for (my $i=0; $i<3;++$i){
	    my $lln = <ANN>;
	}
    }
    while(<ANN>)
    {
      if (defined ($_)) {
	chomp;  
        if ($_ ne "") {
	    $str = $_;
	    $str =~ s/^\s+//;
	    my @in = split (/\s+/,$str,9);
	    #print "in0 $in[1]\n";
	    if ($lfile =~ /\.rnt/){
		my $lname = $in[0];
                $lname =~ s/\.\./-/;
		$in[3] = $lname;
                my $nstr = $in[0];
                for (my $j = 1; $j < @in; ++$j) {
		   $nstr .= "\t$in[$j]";
	        }
		$str = $nstr;
	    }
            my @loc = split (/\.+/,$in[0]);
	    if (!defined($a{$loc[1]})) {
		$a{$loc[1]} = $str;
	    }else {
		my $n_loc = $loc[1] +1;
		if (!defined($a{$n_loc})){
		    $a{$n_loc} = $str;
		}else {
		    $n_loc = $loc[1] -1;
		    if (!defined($a{$n_loc})){
			$a{$n_loc} = $str;
		    }else {
			print "ERR: key $loc[1]\n";
		    }
		}
	    }
	    
	}
      }
  }
  close(ANN);
}

sub printFile {

    if ($ARGV[2]) {
	open (ANN2, ">$out") or die "Can not open $out....exit\n";
    }


    my @arr = keys(%a);
    if ($ARGV[2]) {
	    print ANN2 "$str1$str2$str3";
	}
	else {
	    print "$str1$str2$str3";
	}
    my @sort_arr = sort {$a <=> $b}(@arr);
    for (my $i = 0; $i < @arr; ++$i) {
	if ($ARGV[2]) {
	    print ANN2 "$a{$sort_arr[$i]}\n";
	}
	else {
	    print "$a{$sort_arr[$i]}\n";
	}
    }

    if ($ARGV[2]) {
	close(ANN2);
    }
}
