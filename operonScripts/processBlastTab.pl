#!/usr/local/bin/perl -w
# this is used to process blast table without header (-m 8)

my $file = $ARGV[0]; ##blast table
my $exfile = $ARGV[1]; ##table of genePID, NC # and genome #
my $newfile = $ARGV[2]; ##output

my %tab;
open (ANN1,"<$exfile") or die "can not open $exfile\n";
while (<ANN1>){
   chomp;
   my @arr = split (/\s/,$_);
   my $line = $arr[1] . "," .$arr[2] ."," .$arr[3] .",".$arr[4];
   $tab{$arr[0]} = $line;


}
close (ANN1);
open (OUT, ">$newfile") or die "can not open $newfile\n";
if ($file !~ /\.gz/) {
    open (ANN, "<$file") or die "can not open $file\n";
}else {
     open(ANN, "gunzip -c $file |");
}
my $qgi="";
my @qgihits;
while (<ANN>){
    chomp;
    my $line = $_;
    my @arr = split (/\t/,$line);
    my $eval = $arr[10];
    my @lar = split (/\|/,$arr[0]);
    my $curqgi = $lar[1];
    my @lar2 = split(/\|/,$arr[1]);
    my $curhit = $lar2[1];
    if (defined($tab{$curhit}) &&($tab{$curhit} ne "")) {
        my $nline = "(" .$curhit . "," .$tab{$curhit} ."," . $eval .")";
        if ($qgi eq ""){
	   $qgi = $curqgi;
	   push (@qgihits,$nline);	
        }elsif ($qgi eq $curqgi){
           push (@qgihits,$nline);
        }else{
           print_qgi();
           $qgi = $curqgi;
	   push (@qgihits,$nline);
        }
    }
}
close(ANN);
print_qgi();
close(OUT);
sub print_qgi{

        my $line = "(" . $qgi . "," .$tab{$qgi} . ")";
	print OUT "$line\t";
	for (my $i =0; $i < @qgihits; ++$i) {
	   print OUT "$qgihits[$i]\t";
	}
        print OUT "\n";
	@qgihits=();
   
}
