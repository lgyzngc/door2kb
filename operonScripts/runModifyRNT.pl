#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $ptt = "\/gpfs0\/home\/staff\/phd\/pttMay08";
   my $program = "modifyRNT_file.pl";

   my $name = $ARGV[0];          #sequence file
   ####################################################################
  printjobfile($name);


sub printjobfile{
     my $NC_name = shift();
     my $lnr = "\/scratch\/phd\/$NC_name";
     my $seqout="/scratch/phd/$NC_name.mod";
     my $cmdfile="$dir\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "scp $ptt\/$NC_name /scratch/phd/ \n";
     print JOBFILE "perl $dir\/$program $lnr \>$seqout\n";
     print JOBFILE "scp $seqout $ptt\/\n";
     print JOBFILE "rm -rf $seqout\n";
     print JOBFILE "rm -rf /scratch/phd/$NC_name\n"; 
    close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.1);
}
