#!/usr/bin/perl
use POSIX;
# use phyloTable contain the gene position in each genome
# map the gene to its homolog in another genome with a try to maximize the conservedness of the region
## Need to do each NC separate because the genes will be order by the position in the genome
my $phy_file = $ARGV[0];
my $pairFile = $ARGV[1];
my @pairs;
my @genomes;
my @genes;
my %cur_genome_phy;
my %gene_phy;
my $nan = "NaN"; ## value to print out when not found
my @oriOP;
my $numGenome =0;
my @temPos;
my @curPos;
my @curGenome;
my @curGenome2;
my @result;
my $K = 1; ### distance between two genes
if (@ARGV <1) {
    print "Usage : phylo_file pairFile\n";
    die;
}

processPhyfile();
processPairfile();
for (my $i = 0; $i < @pairs; ++$i) {
    my @la3 = split (/\s/,$pairs[$i]);

    getCurGenome($la3[0]);
	getCurGenome2($la3[1]);
        my $line = $pairs[$i] . " " . runList();	
        push (@result,$line);
	print "$line\n";
    
}

sub getCurGenome{
    my $name = shift();
    @curGenome = split(/\s/,$gene_phy{$name});
}

sub getCurGenome2{
     my $name = shift();
    @curGenome2 = split(/\s/,$gene_phy{$name});
}
################
sub runList {
    my @larr;
    for (my $i = 1; $i < @curGenome; ++$i) {
	my $curHom = $curGenome[$i];
	my $curHom2 = $curGenome2[$i];
	if (($curHom ne "0")&&($curHom =~ m/,/)) {
	    my @minPrev;
	    my @curPos2;

	    @temPos=();
	    getPos($curHom);
	    @curPos = @temPos;
	    #print "curHom1 $curHom curHom2 $curHom2\n";
	    if (($curHom2 ne "0")&&($curHom2 =~ m/,/)) {
		@temPos=();
		getPos($curHom2);
                @curPos2 = @temPos;
		#print "curPos2 @curPos2\n";
		@temPos=();
		getDist(@curPos2);
		@minPrev = @temPos;
	    }else {
		$curPos2[0] = $curHom2;
	    }
	    if ((@minPrev >2)) { ## no homolog in the adjacent genes
		#print "result $minPrev[2]\n";
	     
		push(@larr,$minPrev[2]);
	    }
	}
	
    }
    if (scalar(@larr) == 0) {
	return $nan;
    }else {
	my @la2 = sort{$a<=>$b}@larr;
	my $stop = 0;
	my $val = 0;
	for (my $li = 0; ($li < @la2) &&($stop ==0); ++$li) {
	    if ($la2[$li] == 1){
		++$val;
	    }elsif ($la2[$li] > 1) {
		++$stop;
	    }
	}
	return $val;
    }
  
 
}##end sub runList

sub getDist {
    my $mindis = 10000;
    my $minInd1;
    my $minInd2;
    my @lar =@_;
  # print "inside getDist @lar @curPos\n"; 
    for (my $k = 0; $k < @lar; ++$k) {
	#print "k $k\n";
	for (my $q = 0; $q < @curPos; ++$q) {
	    #print "q $q\n";
	    if ($lar[$k] == $curPos[$q])  { ## same NC
		if (($k == 0) &&($q ==0)){
		    $minInd1 = $k+1;
		    $minInd2 = $q+1;
		    $mindis = abs($lar[($k+1)] - $curPos[($q+1)]);
		}else {
		    my $lval = abs($lar[($k+1)] - $curPos[($q+1)]);
		    if ($lval < $mindis) {
			$minInd1 = $k+1;
			$minInd2 = $q+1;
			$mindis = $lval;
		    }
		}
		
	    }
	    $q = $q+1;
	}
	$k = $k+1;
    }
    push(@temPos,$minInd2, $minInd1,$mindis);

}

sub getPos {
    my $lstr= shift();
    my @lar = split(/_/,$lstr); ## _ as delimitor for gene
    for (my $j = 0; $j <@lar; ++$j) {
	my @llar = split (/,/,$lar[$j]);
	push (@temPos,$llar[1], $llar[4]);
    }
    
}
##################################
sub checkEvalue {
    my $l_arr = shift;
    if ($l_arr =~ /^e/){
	$l_arr = "1" .$l_arr;
    }
    if ($evalue =~ /^e/) {
	$evalue = "1" . $evalue;
    }
    if ($l_arr <= $evalue) {
	return 1;
    }else {
	return 0;
    }
}


###################
##PROCESS PHYLO FILE
sub processPhyfile {
    if ($phy_file =~ /\.gz/){
	open (INFILE, "gunzip -c $phy_file |" ); ##OPEN FROM A ZIP FILE
    }elsif (-e "$phy_file") {
	open INFILE, "$phy_file";
    }else {
	die ("Cannot open phy_file  $phy_file\n");
    }
    while (<INFILE>){
	my $str = $_;
	chomp($str);
	my @l_arr = split (/\s/,$str);	
	my @arr1 = split(/\,/,$l_arr[0]);
	$arr1[0] =~ s/[(,)]//g;
        push(@genes,$arr1[0]);
	$gene_phy{$arr1[0]} = $str;
	if ($numGenome == 0) {
	    $numGenome = scalar(@l_arr) -1 ;
	}elsif ($numGenome != (scalar(@l_arr) -1)){
	    print "ERR: $phy_file: not equal # of column gene: $arr1[0]\n";
	}    
    }
    close (INFILE);
}

##process pairFile
sub processPairfile {
	if (-e $pairFile) {
	   open (PAIR, "$pairFile");
		while (<PAIR>) {
		   chomp;
		   push (@pairs, $_);
	
               }
	}else {
	   die ("Cannot open $pairFile\n");
	} 
	close (PAIR);
}
