#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
my $subdir = "neiDist";
  my $genePairdir = "genePairs";
   my $homologdir = "homologs";
   my $evalue = 1e-20;
   my $genomeNum = 1225;
   my $name = $ARGV[0];          #sequence file
   ####################################################################
  printjobfile($name);


sub printjobfile{
     my $NC_name = shift();
     my $NC_pair = $NC_name .".adjPair";
     my $NC_hom = $NC_name . ".ffn.hom";
     my $frn = $NC_name . ".frn.hom";
     my $seqout="/scratch/phd/$NC_name.neiDist";
     my $out1 = "/scratch/phd/$NC_name.phy";
     my $cmdfile="$dir\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";


     print JOBFILE "if [ -e $dir\/$homologdir\/$frn ]; then cat $dir\/$homologdir\/$NC_hom $dir\/$homologdir\/$frn \> /scratch/phd/$NC_hom; fi \n";
     print JOBFILE "if [ ! -e $dir\/$homologdir\/$frn ]; then scp $dir\/$homologdir\/$NC_hom /scratch/phd/; fi \n";

     print JOBFILE "perl $dir\/extract_phylo_wEcutoff_ffn_printGI.pl \/scratch\/phd\/$NC_hom $evalue $genomeNum \> $out1\n";
     print JOBFILE "perl $dir\/calculatePairwiseHomologAdjacent_ffn.pl $out1 $dir\/$genePairdir\/$NC_pair \>$seqout\n";
     print JOBFILE "scp $seqout $dir\/$subdir\/\n";
     print JOBFILE "rm -rf \/scratch\/phd\/$NC_name*\n";

     close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
