#!/usr/local/bin/perl -w
#print commands to run large number of files
   my $linx = "ppc64linux";
   my $database = "\/gpfs0\/home\/staff\/phd\/bacPTT";
   my $name = $ARGV[0];          #sequence file
   my $leng = 300;
   ####################################################################
  printjobfile($name);
sub printjobfile{
     my $NC_name = shift();
     my $fna = "$NC_name.". "fna";
     my $ptt = "$NC_name" . ".ptt_rnt.sorted";
     my $out1 = $NC_name .".prom300";
     my $sdir = "\/scratch\/phd";
     my $cmdfile="$database\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";
     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "scp $database\/fna\/$fna /scratch/phd/ \n";
     print JOBFILE "scp $database\/ptt\/$ptt $sdir\/\n";
     print JOBFILE "scp $database\/run_cutUpstream.pl $sdir\/\n"; 
     print JOBFILE "cd $sdir\n";
     print JOBFILE "perl $sdir\/run_cutUpstream.pl $sdir\/$ptt $sdir\/$fna $leng \>$sdir\/$out1\n";
     print JOBFILE "scp $sdir\/$out1 $database\/\n";
     print JOBFILE "rm -rf $sdir/$NC_name.*\n";
    close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
