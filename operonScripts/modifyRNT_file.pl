#!/usr/local/bin/perl -w

## SORT THE FILE numerically according to the first column

my $inputfile = $ARGV[0]; ## name of the file to sort
my $out = $ARGV[1]; 
my %a;
my $str;

if ((!$ARGV[0])) {
   die "Not enough parameters: inputfile....exit\n";
}
open (ANN, "<$inputfile") or die "Can not open $inputfile....exit\n";
my $str1 = <ANN>;
my $str2 = <ANN>;
my $str3 = <ANN>;
while(<ANN>)
{
    if (defined ($_)) {
	chomp;  
        if ($_ ne "") {
	    $str = $_;
	    $str =~ s/^\s+//;
	    my @in = split (/\s+/,$str,9);
	    my $lval = $in[0];
	    $lval =~ s/\.+/-/;
	    $in[3] = $lval;
	    for (my $i = 0; $i<@in; ++$i) {
		print "$in[$i]\t";
	    }
	    print "\n";
	}
    }
}
close(ANN);

