#!/usr/local/bin/perl -w

# This program is used to print pairs of genes in an operon

my $inFile = $ARGV[0]; # file with pairs of neighbors
my $out = $ARGV[1];
   ####################################################################
open (ANN, "<$inFile") or die "Can not open $inFile ... exit\n";
open (OUT, ">$out") or die "Can not open $out ... exit\n";
while (<ANN>) {
    my $str = $_;
    chomp ($str);   
    $str =~ s/^\s+//; ##REMOVE SPACE AT THE START   
    $str =~ s/\Z\s+//; ##REMOVE SPACE AT THE END 
  
    my @arr = split (/\s/, $str);
    my $leng = @arr;
    for (my $i = 0; $i < ($leng -1) ; ++$i){
	if (($i+2) < ($leng-1)){
	    print OUT "$arr[$i] $arr[$i+2]\n";
	}else {
	    print OUT "$arr[$i] nd\n";
	}
    }
}
close(ANN);
close (OUT);
