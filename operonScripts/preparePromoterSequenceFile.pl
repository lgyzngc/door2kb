#!/usr/local/bin/perl -w

#this script will calculate features for the operon prediction
# it will need a faa file, ptt file and sequence file and a database of available microbe genomes for comparison
# it will do:
#3. print the directon from ptt file
#4. print adjacent pair from the directon
#6. calculate IG between adj pair
#8. calculate the motif frequency in the promoter region from fna file

my $inFile = $ARGV[0];          # This is the file contain NC number

my $scriptpath ="/md3000/staff/maqin/operonScripts";
my $path ="/md3000/staff/maqin";

#input file
my $outpath = $path . "/$inFile";
my $ptt = "$inFile" .".ptt";
my $faa = "$inFile" .".faa";
my $fna = "$inFile" .".fna";
my $iglengcutoff = 50;
#directon
my $printdirecton = "$scriptpath/print_directon_from_ptt_gi.pl";
my $ncdirecton = $inFile .".directon";

#adj pair &next pair ig, length ratio
my $printAdjPair = "$scriptpath/print_pair_in_operon.pl";
my $ncAdjPair = $inFile . ".adjPair";
my $runIG = "$scriptpath/print_IG_between_pair_with_gi.pl";
my $ncig = $inFile . ".ig";

#motif
my $motif = "$scriptpath/runCalMotif.pl";
my $mot = "upstreamMotif";
my $promLength = 300;
my $ncmotif = $inFile .".motif";

my $scratchdir = "/scratch/phd/$inFile/";
if (-e "$scratchdir"){
    system "rm $scratchdir/*\n";
}else {
    system "mkdir -p /scratch/phd/$inFile\n";
}
#system "cd $scratchdir\n";
runDirecton();
runAdjpair();
runMotif();

system "rm $scratchdir/*\n";
system "rmdir $scratchdir\n";


sub runMotif {
    my $sdir = "$path/$inFile";
    system "perl $scriptpath\/cutUPstreamGenome.pl $sdir/$ptt $sdir\/$fna $promLength \>$scratchdir\/$inFile.prom\n"; ##cut upstream of whole genome
    system "perl $scriptpath\/print_downstreamGI_lengthcutoff.pl $sdir/$ncig $iglengcutoff \> $scratchdir\/$inFile.downstream\n"; ## print the downstream gene in the gene pair
    system"perl $scriptpath\/printSelectMotif.pl $scratchdir\/$inFile.downstream $scratchdir\/$inFile.prom \> $scratchdir/$ncmotif\n"; ## print the promoter of the downstream gene
    system "scp $scratchdir/$inFile.* $sdir/\n";
}

sub runAdjpair {
    if (-e "$path/$inFile/$ncdirecton") {
	system "perl $printAdjPair $path/$inFile/$ncdirecton $path/$inFile/$ncAdjPair\n";
	system "perl $runIG $path/$inFile/$ptt $path/$inFile/$ncAdjPair $path/$inFile/$ncig\n";
	
    }
}

sub runDirecton {
    system "perl $printdirecton $path/$inFile/$ptt $path/$inFile/$ncdirecton\n";
}

