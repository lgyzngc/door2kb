#!/usr/local/bin/perl -w

# This program is used to calculate the phylogenic distance of a gene pairs for operon prediction
# The formula for the distance taken from Chen et al paper
# d= L - (L-d(H))sqrt(E(p))
# p : percentage of identity between 2 profile that is 0
# d(h) hamming distance between 2 profile (total differences  at each position)
# E(p) = -plog(p) -(1-1)log(1-p) ## will use natural log
# The phylogenetic profile will be 0 and 1 string, each separate by a space, first column is gene name
# the gene pair file have each pair of gene in one line, separated by a space

my %micro; # contain phylo data as a hash table of arrays with key is the gene name
my @gene; # the gene name, used as key for the hash table
my $pair; # current pair of genes
my @val1; # phylo of gene1
my @val2; # phylo of gene2
my $leng; #leng of the phylo array
my @pair_gene; #contain all gene pairs
my @result_arr; #contain the result of each pair-gene calculations
my $abst = 0; ##value indicated gene absence
my $inFile = $ARGV[0];          # This is the phylo file
my $pairFile = $ARGV[1];          # This is the pair of gene list
my $out = $ARGV[2];

my $nan = "NaN";   
   ####################################################################
if ((@ARGV <2 )) {
    print "usage phylo-file pairFile\n";
}else {
    open (OUT, ">$out");
    process_infile();
    process_pairFile();
    cal_dist();
    #my @col_key =sort( keys (%mean));
    #for (my $k =1; $k <@col_key; ++$k) {
	#my $l_key = $col_key[$k];
	#print "$col_key[$k] $mean{$l_key} $stddev{$l_key}\n";
    #} 

    #foreach (@gene) {
	#my $name = $_;
	#my $rank = $name . "rank";
	#print "$_\n@$name\n@$rank\n";
    #}
    close(OUT);
} ## end of main

sub process_infile {
    open (ANN, "<$inFile") or die "Can not open $inFile ... exit\n";

    while (<ANN>) {
	my $str = $_; 
	chomp ($str);  
	$str =~ s/^\s//; ##REMOVE SPACE AT THE START   
	$str =~ s/\Z\s//; ##REMOVE SPACE AT THE END   
	my @arr = split (/\s/, $str);
	my $name = $arr[0];
	#$name =~ s/B/b/g;
	push (@gene, $name);
	shift (@arr);
	##put the data into gene array 
	@$name = @arr;
	
    }
    close(ANN);
}
sub process_pairFile {
    open (ANN, "<$pairFile") or die "Can not open $pairFile ... exit\n";
    while (<ANN>) {
	chomp;
	if ((defined $_) && ($_ =~ /\w/)){ 
	    #$_ =~ s/B/b/g;
	    push (@pair_gene, $_);
	}    
    }
    close(ANN);
}

sub cal_dist {
    #print "gene1 gene2 ham score\n";
    for (my $ind = 0; $ind < @pair_gene; ++$ind) {
	@result_arr =();
	$pair = $pair_gene[$ind];
	my $gene1;
	my $gene2;
	($gene1, $gene2) = split (/\s/,$pair);
	$gene1 =~ s/\s//g;
	$gene2 =~ s/\s//g;
	if (($gene1 eq "nd")||($gene2 eq "nd")) {
	    print OUT "$gene1 $gene2 $nan $nan\n";
	}else {
	    @val1 = @$gene1;
	    @val2 = @$gene2;
	    if ((@val1> 0) && (@val2 > 0)) {	    
		my $leng1 = @val1;
		my $leng2 = @val2;
		if ($leng1 > $leng2) {
		    $leng = $leng1;
		}else {
		    $leng = $leng2;
		}
		cal_phylo();
	    
		print OUT "$gene1 $gene2 @result_arr\n";
      
	    }else {
		print OUT "$gene1 $gene2 $nan $nan\n";
	    }
	}
    }
}

sub cal_phylo {
    my $ham_dist = 0;
    my $zero_id = 0;
    my $total_id = 0;
    my $dis ;
    my $E = 0;
    my $bef;
    #print "leng $leng\n";
    for (my $i = 0; $i < $leng; ++$i ) {
	if ($val1[$i] eq $val2[$i]) {
	    ++$total_id;
	    if ($val1[$i] == $abst) {
		++$zero_id;
	    }
	}else {
	    ++$ham_dist;
	}	
    }
    $bef = $zero_id;
    if ($total_id > 0) {
	$zero_id = ($zero_id/$total_id); 
	if ($zero_id == 0) {
	    $zero_id = 0.0000000001;
	}elsif ($zero_id == 1) {
	    $zero_id = 0.9999999999;
	}
	#print "ham_dist $ham_dist zero $bef after $zero_id total_id $total_id\n";
	$E = -$zero_id*log($zero_id) - (1-$zero_id)*log(1-$zero_id);
	$dis = $leng - ($leng - $ham_dist)*sqrt($E);
	#print "E $E dis $dis\n";
	push (@result_arr, $ham_dist, $dis);
	return $dis;
    }else {
	push (@result_arr, $ham_dist, $leng);
	return $leng;
    }
}
