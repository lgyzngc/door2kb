#!/usr/bin/perl -w
use FindBin qw($Bin);

#this script will calculate features for the operon prediction
# it will need a faa file, ptt file and sequence file and a database of available microbe genomes for comparison
# it will do:
#1. blast search of the target genome against the microdatabase
#2. process the blast file
#3. print the directon from ptt file
#4. print adjacent pair from the directon
#5. print next pair from directon
#6. calculate IG between adj pair
#7. calculate genelength ratio between adj pair
#8. calculate the motif frequency in the promoter region from fna file
#9. calculate the phylo distance
#10 calculate the neighbor distance

my $inFile = $ARGV[0];          # This is the file contain NC number
if ($inFile =~ /(.*)\/$/) { $inFile = $1; }
my $fna = $inFile .".fna";
my $ptt = $inFile . ".ptt";
my $faa = $inFile . ".faa";
my $err = $inFile . ".err";

#my $microbedbpath= "/md3000/staff/maqin/operonPRED/microbe110109_faaDB";
#my $scriptpath ="/md3000/staff/maqin/operonPRED/operonScripts";
##my $path ="/md3000/staff/maqin";
#my $path ="$Bin/..";
#my $microbedb="microbe110109_faa";
#my $blastpath = "/md3000/tools/i386/blast";
#my $linx = "glinux";

my $microbedbpath= "$Bin/../microbe110109_faaDB";
my $scriptpath = $Bin;
#my $path ="/md3000/staff/maqin";
my $path ="$Bin/..";
my $microbedb="microbe110109_faa";
my $blastpath = "$path/../blast/bin/blastall";
my $linx = "glinux";

open ERR, ">$path/$inFile/$err" or die "Cann't open >$path/$inFile/$err for write: $!";

my $cmdfile="$inFile.cmd.sh";
if (!(-e "$path/$inFile/$fna")){
    print ERR "fna file not found\n";
}elsif (!(-e "$path/$inFile/$faa")){
    print ERR "faa file not found\n";
}elsif (!(-e "$path/$inFile/$ptt")){
    print ERR "ptt file not found\n";
}else {

    printjobfile();
}
close (ERR);
    
sub printjobfile{
     
     open JOBFILE,">$path/$inFile/$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "perl $scriptpath\/prepareFilesFinal.pl $inFile\n";
     close JOBFILE;
     system("chmod 755 $path/$inFile/$cmdfile");

     system("$path/$inFile/$cmdfile");
     #sleep(30);
}


