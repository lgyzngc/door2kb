#!/usr/local/bin/perl -w

# This program is used to calculate the phylogenic distance of a gene pairs for operon prediction
# The formula for the distance taken from Chen et al paper
# d= L - (L-d(H))sqrt(E(p))
# p : percentage of identity between 2 profile that is 0
# d(h) hamming distance between 2 profile (total differences  at each position)
# E(p) = -plog(p) -(1-p)log(1-p) ## will use natural log
# The phylogenetic profile will be 0 and 1 string, each separate by a space, first column is gene name
# the gene pair file have each pair of gene in one line, separated by a space

my %micro; # contain phylo data as a hash table of arrays with key is the gene name
my @gene; # the gene name, used as key for the hash table
my $pair; # current pair of genes
my @val1; # phylo of gene1
my @val2; # phylo of gene2
my $leng = 675;
my @pair_gene; #contain all gene pairs
my @result_arr; #contain the result of each pair-gene calculations
my $abst = 0; ##value indicated gene absence
my $inFile = $ARGV[0];          # This is the phylo file
my $pairFile = $ARGV[1];          # This is the pair of gene list
my $nan = "NaN";   
my %phytab;
   ####################################################################
if ((@ARGV <2 )) {
    print "usage phylo-file pairFile\n";
}else {
    process_infile();
    process_pairFile();
    #cal_dist();
} ## end of main

sub process_infile {
    open (ANN, "<$inFile") or die "Can not open $inFile ... exit\n";

    while (<ANN>) {
	my $str = $_; 

	chomp ($str);
	$str =~ s/^\s//; ##REMOVE SPACE AT THE START   
	$str =~ s/\Z\s//; ##REMOVE SPACE AT THE END   
        if ((defined($str)) && ($str ne "")){  
	    
	    my @arr = split (/\s/, $str);
	    my $name = $arr[0];
	    push (@gene, $name);
	    #shift (@arr);
	    #@$name = @arr;
	    my $lleng = length($name)+1;
	    if ($lleng > length($str)) {
		$phytab{$name} = $nan;
	    }else {
		$str = substr($str,$lleng);
		$phytab{$name} = $str;
	    }
	}
	
    }
    close(ANN);
}
sub process_pairFile {
    open (ANN, "<$pairFile") or die "Can not open $pairFile ... exit\n";
    while (<ANN>) {
	chomp;
	if ((defined $_) && ($_ =~ /\w/)){ 
	    @pair_gene=();
	    push (@pair_gene, $_);
	    cal_dist();
	}    
    }
    close(ANN);
}

sub cal_dist {
    #print "gene1 gene2 ham score\n";
    for (my $ind = 0; $ind < @pair_gene; ++$ind) {
	@result_arr =();
	$pair = $pair_gene[$ind];
	my $gene1;
	my $gene2;
	($gene1, $gene2) = split (/\s/,$pair);
	$gene1 =~ s/\s//g;
	$gene2 =~ s/\s//g;
	if (!defined($phytab{$gene1}) || (!defined($phytab{$gene2}) )) {
	    print "$gene1 $gene2 1\n";
	}elsif (($phytab{$gene1} eq $nan)||($phytab{$gene2} eq $nan)) {
	    print "$gene1 $gene2 1\n";
	}else {
	    @val1 = split(/\s/,$phytab{$gene1});
	    @val2 = split(/\s/,$phytab{$gene2});
	    my @temp1;
            my @temp2;
	    for (my $k = 0; $k < $leng; ++$k) {
		if (defined ($val1[$k]) && ($val1[$k] =~ m/\d/)) {
		    $temp1[($val1[$k])] = 1;
		}
		if (defined ($val2[$k]) && ($val2[$k] =~ m/\d/)) {
		    $temp2[($val2[$k])] = 1;
		    
		}
	    }
	    for (my $k = 0; $k < $leng; ++$k) {
		if (!defined ($temp1[$k])) {
		    $temp1[$k] = 0;
		}
		if (!defined ($temp2[$k])) {
		    $temp2[$k] = 0;
		    
		}
	    }
	    @val2 = @temp2;
	    @val1 = @temp1;
	    if ((@val1> 0) && (@val2 > 0)) {	    
		cal_phylo();
	    
		print "$gene1 $gene2 @result_arr\n";
      
	    }else {
		print "$gene1 $gene2 $nan $nan\n";
	    }
	}
    }
}

sub cal_phylo {
    my $ham_dist = 0;
    my $zero_id = 0;
    my $total_id = 0;
    my $dis ;
    my $E = 0;
    my $bef;
    for (my $i = 0; $i < $leng; ++$i ) {
#	print "$i\n";
	if ($val1[$i] eq $val2[$i]) {
	    ++$total_id;
	    if ($val1[$i] == $abst) {
		++$zero_id;
	    }
	}else {
	    ++$ham_dist;
	}	
    }
    my $lpre = $total_id -$zero_id;
    push (@result_arr, $lpre);
    return $lpre;
    
}
