#!/usr/local/bin/perl -w

# This program is used to print directon from ptt file
# Directon: stretch of genes transcribed in the same direction
# without intervening genes in the other strand
# 

my @directon;
my $curr_dir;
my $new_dir;

my   $inFile = $ARGV[0];          # This is the first file
   
   ####################################################################
open (ANN, "<$inFile") or die "Can not open $inFile ... exit\n";

 my $str = <ANN>; ##first line
$str = <ANN>;  ##second line
$str = <ANN>;  ##third line

while (<ANN>) {
    $str = $_; ##fourth line and on
    chomp ($str);
    $str =~ s/\t/ /g;   
    $str =~ s/\s+/ /g;    
    $str =~ s/^\s//; ##REMOVE SPACE AT THE START   
    $str =~ s/\Z\s//; ##REMOVE SPACE AT THE END   
    my @arr = split (/\s/, $str);
    
    if (!defined ($curr_dir)) {
	push (@directon, $arr[3]);
	$curr_dir = $arr[1];
    }else {
	$new_dir = $arr[1];
	if ($new_dir eq $curr_dir) {
	    push (@directon, $arr[3]);
	}else {
	    for (my $i= 0; $i <@directon; ++$i) {
		print "$directon[$i]\t";
	    }
	    print "\n";
	    @directon= ();
	    push (@directon, $arr[3]);
	    $curr_dir = $arr[1];
	}
    }	 
}
close(ANN);
for (my $i= 0; $i <@directon; ++$i) {
    print "$directon[$i]\t";
}
print "\n";
