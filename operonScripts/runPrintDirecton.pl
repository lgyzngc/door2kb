#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = "ppc64linux";
   my $database = "\/gpfs0\/home\/staff\/phd\/pttMay08";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $subdir = "directons";

   my $name = $ARGV[0];          #sequence file
   ####################################################################
  printjobfile($name);


sub printjobfile{
     my $NC_name = shift();
     my $ptt = $NC_name . ".ptt_rnt.sorted";
     my $lnr = "\/scratch\/phd\/$ptt";
     my $seqout="/scratch/phd/$NC_name.directon";
     my $cmdfile="$database\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";
     if(-e $seqout){
        system("rm $seqout");
     }
     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "scp $database\/$ptt /scratch/phd/ \n";
     print JOBFILE "perl $dir\/print_directon_from_ptt_gi.pl $lnr \>$seqout\n";
     print JOBFILE "scp $seqout $dir/$subdir\/\n";
     print JOBFILE "rm -rf $seqout\n";
     print JOBFILE "rm -rf /scratch/phd/$ptt\n"; 
    close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
