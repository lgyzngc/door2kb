#!/usr/local/bin/perl -w

# This code is used to calculate stastistics of a genome
# Number of occurences per motif in each DNA sequence
# for each sequence, only take the last 250 nucleotides
# after calculate the frequency of a motif in a sequence
# normalize the frequency based on the expected value of the
# motif in the sequence


my $inputfile = $ARGV[0]; # genome file in Fasta
my $motif_file = $ARGV[1]; # what to be counts
my @geneName;
my %tab;
my @n_keys; #motifs
my @prom_seq; # contain the promoter/sequence
my $orf_seq = "";
my %ACGT;
my @acgt = ('a','c','g','t');
my $genome_length = 0;
my %Evalue;

if (@ARGV <2) {
    print " This code is used to calculate stastistics of a genome\n";
    print " will return the frequency of the motif in each sequence\n";
    print "USAGE: genome-sequences, motif-file\n";
    die;
}

process_seq();
my $ll2 = @prom_seq;
process_motif();
@keys = keys (%tab); ## array of motifs
@n_keys = sort @keys;
$ll2 = @keys;
cal_acgt_freq();
cal_motif_Evalue();
cal_motif();


sub process_seq {
   open (ANN, "<$inputfile") or die "Can not open $inputfile....exit\n";
   my $string = <ANN>;

   while(<ANN>) { 
	## initiate seq to hold the aa sequence
	my $seq = "";
	## get all the lines of the sequence
	if  ((defined ($string)) && ($string ne "") 
	      && ($string =~ m/^>/)) {
	    $seq = $_;
	    #print "seq name $string\n";
	    chomp($string);
	    $string =~ s/>//;
	    my @larr = split (/_/,$string); ##TAKE THE NAME OF THE SEQUENCE, SEE EXAMPLE IN NC_000911.ptt_rnt.sorted.directons.pairs.IGregion
	    $string = $larr[0] ." " .$larr[1];
	    push (@geneName, $string);
	    if (defined($seq) && ($seq ne "")) {
		chomp ($seq);
		$orf_seq = <ANN>;
		if (defined($orf_seq) && ($orf_seq ne "")) {
		    
		    while ((defined ($orf_seq)) && ($orf_seq ne "") 
		       && (!($orf_seq =~ />/))) {
			chomp ($orf_seq);
			$seq .= $orf_seq;
			$orf_seq = <ANN>;			
		    }
		}
		$seq =~ s /\t\n//g;
		$seq =~ s/\W//g;
		$seq =~ tr/ACGT/acgt/;
                my $l_leng = length($seq);
		$genome_length += $l_leng; ## calculate the genome length
		push (@prom_seq, $seq);

		#MAKE SURE DO NOT SKIP LINE
		if ((defined ($orf_seq)) && ($orf_seq ne "") 
		    && ($orf_seq =~ />/)) {
		    $string = $orf_seq;
		}
	    }
	}else {
	    $string = <ANN>;
        }
    }

###END PROCESS FASTA FILE
close(ANN);
} ##END PROCESS_SEQ

sub process_motif {
    open (ANN, "<$motif_file") or die "Can not open $motif_file....exit\n";

    while(<ANN>) {
       my $motif = $_;
       if ((defined ($motif)) && ($motif =~ m/\w/)){
	   chomp ($motif);
	   $motif =~ s/\s//g;
	   $motif =~ tr/ACGT/acgt/;
	   $tab{$motif} = 0;
       }	
    }


    close(ANN);
}###END PROCESS MOTIF FILE

sub cal_motif {

    ## for each sequence, count the number of occurences per motif
    for (my $i = 0; $i < @prom_seq; ++$i) { 
        for  (my $j = 0; $j <@n_keys; ++$j) {
	    $tab{$n_keys[$j]} = cal($i,$n_keys[$j]);
        }
        print "$geneName[$i] ";
        foreach my $var (@n_keys) {
	    printf "%.4f ",$tab{$var};
        }
        print "\n";
     }

} ##END CAL_MOTIF

sub cal {
    my $l_i = shift();
    my $char = shift();
    my $n_match = 0;
    my $l_seq = $prom_seq[$l_i];

##ONLY CALCULATE THE FREQUENCY OF THE LAST 250 NUCLEOTIDES
   my $llseq = length($l_seq);
   if ($llseq > 250) {
       my $ll2 = $llseq - 250;
       $l_seq = substr ($l_seq,$ll2);
   }    

    while ($l_seq =~ m/$char/ig) {
	++$n_match;
   }
   my $l_leng = length ($l_seq);
   my $l_char = length ($char);
   my $l_freq = $n_match/ ($l_leng-$l_char+1);
   
###CALCULATE NORMALIZED FREQUENCY
   my $norm_freq = ($l_freq/$Evalue{$char});
    #print "motif $char occurent $n_match freq $l_freq mot_evalu $Evalue{$char} norm_freq $norm_freq\n";
   return $norm_freq;
}

sub cal_acgt_freq {
    ## for each sequence, count the number of occurences per motif
    for (my $i = 0; $i < @prom_seq; ++$i) { 
        for  (my $j = 0; $j <@acgt; ++$j) {
	    $ACGT{$acgt[$j]} += count_acgt($prom_seq[$i],$acgt[$j]);
        }
    }
    for  (my $j = 0; $j <@acgt; ++$j) {
       $ACGT{$acgt[$j]} = $ACGT{$acgt[$j]}/$genome_length;
    }

} ##END CAL_ACGT_FREQ
sub count_acgt {
    my $l_seq = shift();
    my $char = shift();
    my $n_match = 0;
    
    while ($l_seq =~ m/$char/ig) {
	++$n_match;
    }
    return $n_match;
}
sub cal_motif_Evalue {
    
    foreach $lval (@n_keys) {
       my $pM = 1;
       foreach $lchar (@acgt) {
	   $a = count_acgt ($lval,$lchar);
	   for (my $i = 0; $i <$a; ++$i) {
	       $pM *= $ACGT{$lchar};
	   }
       }
       
       $Evalue{$lval} = $pM;
    }

}
