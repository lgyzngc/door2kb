#!/usr/local/bin/perl -w

# require a ptt file,
# a file of list of gene pairs, each pair in one line

my %micro; # contain ptt file with the gene names as keys
my @gene; # the gene name, used as key for the hash table
my $pair; # current pair of genes
my @val1; # ptt line of gene 1
my @val2; # ptt line of gene 2
my $leng; #leng of the phylo array
my @pair_gene; #contain all gene pairs
my @result_arr; #contain the result of each pair-gene calculations
my $genomeLeng;

my $inFile = $ARGV[0];          # This is the ptt file
my $pairFile = $ARGV[1];          # This is the pair of gene list
my $out =  $ARGV[2];  
   ####################################################################
if ((@ARGV <2 )) {
    print OUT "usage ptt-file pairFile\n";
}else {
    open (OUT, ">$out") or die "Can not open $out ... exit\n";
    process_infile();
    process_pairFile();
    cal_dist();
    close(OUT);
} ## end of main

sub process_infile {
    open (ANN, "<$inFile") or die "Can not open $inFile ... exit\n";
    for (my $i = 0; $i <3; ++$i) {
	my $str = <ANN>;
	if ($i == 0) {
	    my @arr = split (/\.\./,$str);
	    $genomeLeng = $arr[1];
	    $genomeLeng =~ s/\s//g;
	    #print "genomeleng $genomeLeng\n";
	}
    }
    while (<ANN>) {
	my $str = $_; 
	chomp ($str);  
	$str =~ s/^\s+//; ##REMOVE SPACE AT THE START   
	$str =~ s/\Z\s+//; ##REMOVE SPACE AT THE END   
	my @arr = split (/\s+/, $str);
	#print "arr0 $arr[5]\n";
	my $name = $arr[3];
	push (@gene, $name);
	##put the data into gene array 
	@$name = @arr;
	
    }
    close(ANN);
}
#print @gene, "\n";

sub process_pairFile {
    open (ANN, "<$pairFile") or die "Can not open $pairFile ... exit\n";
    while (<ANN>) {
	chomp;
	if ((defined $_) && ($_ =~ /\w/)){ 
	    push (@pair_gene, $_);
	}    
    }
    close(ANN);
}

sub cal_dist {
    #print "gene1 gene2 ham score\n";
    for (my $ind = 0; $ind < @pair_gene; ++$ind) {
	@result_arr =();
	$pair = $pair_gene[$ind];
	my $gene1;
	my $gene2;
	($gene1, $gene2) = split (/\s/,$pair);
	$gene1 =~ s/\s//g;
	$gene2 =~ s/\s//g;
	@val1 = @$gene1;
	@val2 = @$gene2;
	if ((@val1> 0) && (@val2 > 0)) {	    
	    my $leng1 = $val1[2];
	    my $leng2 = $val2[2];
	    my $dir1 = $val1[1];
	    my $dir2 = $val2[1];

	    ###RNA GENE HAS NAME AS THE "START-STOP" POSITION 
	    if (!($gene1 =~ /-/)) {
		$leng1 = $leng1 *3;
	    }
	    if (!($gene2 =~ /-/)) {
		$leng2 = $leng2 *3;
	    }
	    my $rat;
	    if ($dir1 eq $dir2) { 
		if (($dir1 eq "F")|| ($dir1 eq "+")){
		    $rat = $leng1/$leng2;
		}else {
		    $rat = $leng2/$leng1;
		}
	    }
	    if (!defined $rat){
		$rat = "NONE";
	    }
	    print OUT "$gene1 $gene2 $rat\n";
      
	}else {
	    print OUT "$gene1 $gene2 NONE\n";
	}
    }
}

