#!/usr/local/bin/perl -w

#this script will generate feature file for operon prediction using matlab predictor
# the feature file will include the gene ID (2 genes) then the phylogenetic hamming distance, then 
# the phy-ent, length ratio, IG, mot1(threeleter, #13), mot2 (five letter, #1024), neighbor score (simple count);


   my $inFile = $ARGV[0];          # This is the file contain NC number
   my @genomename ;
   $genomename[0] = $inFile;
   my @phy;
   ####################################################################
my $mot1col = 2;
my $mot2col = 3; 
my $path = "\/gpfs0\/home\/staff\/phd\/operonPreMay08\/";  
my $phypath = "phyloDist\/";
my $phytail = ".phyDist";
my $lengpath = "lengRatio\/";
my $lengtail = ".lengRat";
my $igpath = "genomeIG\/";
my $igtail = ".IG";
my $mot1path = "genomeMotif\/";
my $mot1tail = ".motif";
my $mot2path = "genomeMotif\/";
my $mot2tail = ".motif";

my $neipath = "neiDist\/";
my $neitail = ".neiDist";

for (my $i = 0; $i < @genomename; ++$i) {
    @phy =();
    my $printfile = 0;
    my $val = addPhy($genomename[$i]);
    if ($val == 1) {
	$val = addLeng($genomename[$i]);
	if ($val == 1) {
	    $val = addIG($genomename[$i]);
	    if ($val ==1 ) {
		$val = addMot1($genomename[$i]);
		if ($val ==1) {
		    $val = addMot2($genomename[$i]);
		    if($val == 1) {
			$val = addNei($genomename[$i]);
			if ($val == 0){
			    print "error in nei $genomename[$i]\n";
			    ++$printfile;
			}
		    }else{
			print "error in Mot2 $genomename[$i]\n";
			++$printfile;
		    }
		}else{
		    print "error in Mot1 $genomename[$i]\n";
		    ++$printfile;
		}
	    }else{
		print "error in IG $genomename[$i]\n";
		++$printfile;
	    }
	    
	}else{
	    print "error in leng $genomename[$i]\n";
	    ++$printfile;
	}
	
    }else{
	print "error in phy $genomename[$i]\n";
	++$printfile;
    }
    if ($printfile ==0){
	my $outfile = $path . $genomename[$i].".txt";
	open (OUT, ">$outfile");
	for (my $j = 0; $j <@phy; ++$j) {
	    print OUT "$phy[$j]\n";
	}
	close(OUT);
    }
}

sub addPhy {
    my $lname = shift;
    my $file = $path . $phypath . $lname . $phytail;
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@phy,$_);
	}
	return 1;
    } else {
	return 0;
    }
}
sub addLeng {
    my $lname = shift;
    my $file = $path . $lengpath . $lname . $lengtail;
    my @leng ;
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/) && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[2];
	    }else {
		print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print "$file not the same order\n";
	    return 0;
	}else{
	    return 1;
	}
    } else {
	return 0;
    }

}

sub addIG{
    my $lname = shift;
    my $file = $path . $igpath . $lname . $igtail;
    my @leng ;
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/) && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[3];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print "$file not the same order\n";
	    return 0;
	}else{
	    return 1;
	}
    } else {
	return 0;
    }
}

sub addMot1 {
    my $lname = shift;
    my $file = $path . $mot1path . $lname . $mot1tail;
    my @leng ;
    if (-e $file) {
	if ($file =~ /gz/){
	    open(ANN,"gunzip -c $file |" );
	}else{
	    open (ANN, "$file");
	}

	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/)) { ## && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[$mot1col];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print "$file not the same order\n";
	    return 0;
	}else{
	    return 1;
	}
    } else {
	print "file not found $file\n";
	return 0;
    }
}


sub addMot2{
     my $lname = shift;
    my $file = $path . $mot2path . $lname . $mot2tail;
    my @leng ;
    if (-e $file) {
	if ($file =~ /gz/){
	    open(ANN,"gunzip -c $file |" );
	}else{
	    open (ANN, "$file");
	}

	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/)) { ## && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[$mot2col];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print "$file not the same order\n";
	    return 0;
	}else{
	    return 1;
	}
    } else {
	print "file not found $file\n";
	return 0;
    }
}

sub addNei{
     my $lname = shift;
    my $file = $path . $neipath . $lname . $neitail;
    my @leng ;
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/) && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[2];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print "$file not the same order\n";
	    return 0;
	}else{
	    return 1;
	}
    } else {
	return 0;
    }
}
