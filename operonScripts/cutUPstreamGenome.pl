#!/usr/local/bin/perl -w

my $inputfile = $ARGV[0]; # use ptt file
my $datafile  = $ARGV[1]; # contain the sequence DNA
my $length = $ARGV[2];
my $seq ;

if ((!$ARGV[0]) || (!$ARGV[1])) {
   die "Not enough parameters: inputfile, datafile,....exit\n";
}

open (ANN, "<$datafile") or die "Can not open $datafile....exit\n";
my $line;
while($line = <ANN>) {
    chomp($line);
  next if $line =~ /^>/;
  $line =~ s/\s//g;
  $seq .= $line;
}
close(ANN);
#print "$seq\n";
my $leng = scalar($seq);
#print "$leng\n";

open (ANN, "<$inputfile") or die "Can not open $inputfile....exit\n";

##remove first 3 line of ptt file
for (my $i = 0; $i<3; ++$i){
    my $str = <ANN>;
}
while(<ANN>){
    if (defined ($_)) {
	chomp;  
        my $string = $_;
        my @orf = split (/\t/, $string);
        $orf[3] =~ s/\s//g;
	$orf[5] =~ s/\s//g;
	my $name = $orf[3] ."_" .$orf[5];
	my $dir = $orf[1];
	$dir =~ s/\s//g;
	if ($dir eq "+") {
	    $dir = "F";
	}elsif ($dir eq "-") {
	    $dir = "R";
	}
	#print "$orf[0]\n";
	my @arr = split (/\./,$orf[0]);
	my $start = $arr[0];
	$start =~ s/\s//g;
	my $end = $arr[2];
	$end =~ s/\s//g;
	#print "start $start\n";
	#print "end $end\n";
        print ">$name\n";
        my $nstr ="";
	if (($dir eq "F")|| ($dir eq "+")) {
	    my $val = $start - $length -1;
	    if ($val < 0){
		$val = 0;
		$nstr = cutseq (0, ($start-1));
	    }else {
		$nstr = cutseq ($val, $length);
	    }
	    
        }elsif (($dir eq "R") || ($dir eq "-")){
	    my $val = $end;
            $nstr = cutseq ($val, $length);
            my $rseq = reverse $nstr;
	    $rseq =~ tr/actgACTG/tgacTGAC/;
	    $nstr = $rseq;
	}
	print "$nstr\n";
    }
}
close (ANN); 

sub cutseq {
    my $lstart = shift();
    my $lend = shift();
    return substr($seq, $lstart, $lend);

}
