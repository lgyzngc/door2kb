#!/usr/local/bin/perl -w
## use to number the genes in the genome consecutively.
## This is taytored to the ptt files.  It will number the plus
## strand first, then the minus strand(with a negative sign)

## Number genes in ptt files 
my $progname = "NUMBER PTT";
my $fileList; ## a list of ptt files
my @genome_list;
my @plus;
my @minus;

my @gene_gi;
my $out;
my $err= "num_ptt.err";
my $start_index = 1; 
my $stop_index = 0;
my $overlap = 0;
my %genome_leng;
my $check_overlap;
my @curr_arr;

process_input();

open (ERR, ">$err") or die "Can not open $err....exit\n";

process_fileList();

process_ptt_file();

close (ERR);
###############################################################
##END OF MAIN


#############################################################
##PROCESS INPUT PARAMETERS

sub process_input {
    my $problem = 0;
    my $currArgNum = 0;    
    
    if (defined ($ARGV[0])) {
	while ($currArgNum < @ARGV ) {
	    if ($ARGV[$currArgNum] eq "-i") {
                $fileList = $ARGV[++$currArgNum];
                ++$currArgNum;
	    }else {
		print STDERR "$progname: Unknown option \"$ARGV[$currArgNum]\"\n";
		$problem = 1;
                $currArgNum++;
	    }
	}
    }
    # check to be sure that $blastp_file and $outFile are specified:
    unless ($fileList) {
	$problem = 1;
    }
    # if there's a problem, write information on proper command line format
    if ($problem) {
	print STDERR "# Usage: $progname [options]\n";
	print STDERR "# Number the genes in ptt file consecutively\n";
	print STDERR "# [options] are -keyword value pairs as in the following table\n";
	print STDERR "# -keyword  |  preset value  |  purpose\n";
	print STDERR "#--------------------------------------------------------------------------\n";
	print STDERR "# -i        |  none          |  List files in .ptt format\n";
	print STDERR "# \n";
	print STDERR "### NOTE: -i must be specified ###\n";
	die (" ");
    }
}

###################################################################
##PROCESS FILELIST
sub process_fileList{
    if (-e "$fileList") {
	open (ANN, "<$fileList") or die "Can not open $fileList....exit\n";
	while(<ANN>){
	    if (defined ($_)) {
		chomp;  
		if ($_ ne "") {
		    $str = $_;
		    push (@genome_list, $str);
		}
	    }
	}
	close(ANN);
    }else {
	print ERR "fileList $fileList not exist\n";
    }
} ##END OF PROCESS_FILELIST
  
##################################################################
##PROCESS EACH ptt file
sub process_ptt_file {

    my $inputfile; 

    for (my $i = 0; $i < @genome_list; ++$i) {
    	my $ptt_file = $genome_list[$i];
	$ptt_file =~ s/\s//g;
	my $genome_mat = $genome_list[$i];
	my $out_file = $ptt_file;
        $out_file =~ s/ptt_rnt\.sorted/directon/;
	@plus =();
	@minus = ();
	
	################################
	## PROCESS INDIVIDUAL PTT FILE

	if (-e "$ptt_file") {
	    open (ANN1, "<$ptt_file") or die "Can not open $ptt_file....exit\n";
	    open (OUT, ">$out_file") or die "Can not open $out_file....exit\n";
	    ##fill the hash table genome length
	    my $str1 = <ANN1>;
	    my $str2 = <ANN1>;
	    my $str3 = <ANN1>;
	    print OUT "$str1$str2";
	    print OUT "Directon\tstart\tstop\tdirection\tGInumber\n";
	    my $start = 0;
	    my $curDir;
	    my $newDir;
	    my $dirNum = 0;
	    while(<ANN1>){
		if (defined ($_)) {
		    chomp;  
		    if ($_ ne "") {
			
			$str = $_;
			$str =~ s/\s+/ /g;
			my @arr1 = split (/\s/,$str);
			if ($arr1[0] =~ /join/) {
			   $arr1[0] =~ s /join//;
			   $arr1[0] =~ s /[(,)]//g;
			   my @in_arr = split (/\.+/,$arr1[0]);
			   $arr1[0] = $in_arr[0] . "\t" .$in_arr[2];
			}
			my $l_ln = $arr1[0] ."\t" .$arr1[1] ."\t" .$arr1[3];
			$l_ln =~ s/\.+/\t/;
			if ($start ==0) {
			    $start =1;
			    $curDir = $arr1[1];
			    $dirNum = 1;
			}
			if ($arr1[1] eq $curDir){
			    print OUT "$dirNum\t$l_ln\n";
			}else {
			    ++$dirNum;
			    $curDir = $arr1[1];
			    print OUT "$dirNum\t$l_ln\n";
			}
		    }		    
		}
	    }
	    
	    close (OUT);
	}else {
	    print ERR "ptt_file $ptt_file not exist\n";
	}
	close (ANN1);

    }
} ## END OF PROCESS NEIGHBOR MATRIX
