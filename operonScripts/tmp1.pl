#!/usr/local/bin/perl -w
# this is used to process blast table without header (-m 8)

my $file = $ARGV[0]; ##blast table
my $exfile = $ARGV[1]; ##table of genePID, NC # and genome #
my $newfile = $ARGV[2]; ##output

my %tab;
open (ANN1,"<$exfile") or die "can not open $exfile\n";
while (<ANN1>){
   chomp;
   my @arr = split (/\s/,$_);
   my $line ="";
   for (my $j = 2; $j < @arr; ++$j){
       if ($j == 2) {
	   $line = $arr[$j];
       }else {
	   $line = $line . "," .$arr[$j];
       }
   }
   my $lname = $arr[2];
   $tab{$lname} = $line;

}
close (ANN1);
open (OUT, ">$newfile") or die "can not open $newfile\n";
if ($file !~ /\.gz/) {
    open (ANN, "<$file") or die "can not open $file\n";
}else {
     open(ANN, "gunzip -c $file |");
}
my $qgi="";
my @qgihits;
while (<ANN>){
    chomp;
    my $line = $_;
    my @arr = split (/\t/,$line);
    my $evall = $arr[11];
    my @lar = split (/\|/,$arr[0]);
    my $curqgi;
    my $ncqgi;
    my $ncqhit;
    #if ($lar[0] =~ m/ref/) {
    #    $curqgi = $lar[2];
#	$ncqgi = $lar[1];
 #   }else {

	$curqgi = $lar[1]; ### Need to check ###
	$ncqgi = $lar[0];
	
#    }
    my @lar2 = split(/\|/,$arr[1]);
    my $curhit;
    #if ($lar2[0] =~ m/ref/) {
    #    $curhit = $lar2[2];
#	$ncqhit = $lar2[1];
 #   }else {

	$curhit = $lar2[1]; ### Need to check ###
	$ncqhit = $lar2[0];
 #   }
   
    my $curqgiName = $curqgi;
    my $curhitName = $curhit;
    
    #print "curhit $curhit $curhitName $tab{$curhitName}\n";
    #print "curqgi $curqgi $curqgiName $tab{$curqgiName}\n";

    if (defined($tab{$curhitName}) &&($tab{$curhitName} ne "")) {
        my $nline = "(" .$curhitName . "," .$tab{$curhitName} ."," . $evall .")";
        if ($qgi eq ""){
	   $qgi = $curqgiName;
	   push (@qgihits,$nline);	
        }elsif ($qgi eq $curqgiName){
           push (@qgihits,$nline);
        }else{
           print_qgi();
           $qgi = $curqgiName;
	   push (@qgihits,$nline);
        }
    }
}
close(ANN);
print_qgi();
close(OUT);
sub print_qgi{

        my $line = "(" . $qgi . "," .$tab{$qgi} . ")";
	print OUT "$line\t";
	for (my $i =0; $i < @qgihits; ++$i) {
	   print OUT "$qgihits[$i]\t";
	}
        print OUT "\n";
	@qgihits=();
   
}

sub rmTail{
    my $lit = shift();
    my @lll = split (/\./,$lit);
    return $lll[0];
}
sub rmNC {
    my $lit = shift();
    my @lll = split (/\:/,$lit);
	return $lll[1];
}

sub reverseStrand{
    my $lit = shift();
    my @lll = split (/_/,$lit) ;
    my @llll = split (/-/,$lll[0]);
    return ("c" . $llll[1] . "-" .$llll[0] ."_" . $lll[1] . "_" .$lll[2]);
}

sub rmSplit{
     my $lit = shift();
     my @lll = split (/\,/,$lit);
     return $lll[0];
}
