#!/usr/local/bin/perl -w

#print commands to run large number of files
   my $blastpath = $ARGV[0];
   my $linx = $ARGV[1];
   my $databasepath = $ARGV[2];
   my $seqpath = $ARGV[3];
   my $neval = $ARGV[4];
   my $b = $ARGV[5];
   my $name = $ARGV[6];          #sequence file
   my $microbedb = $ARGV[7];
   ####################################################################
   my $NC_name = $name . ".faa";
   printjobfile();


sub printjobfile{
 
     my $seqout="/scratch/phd/$NC_name.blast";
     my $seqhom="$seqpath/$NC_name.blast";
     my $cmdfile="$seqpath/$NC_name.blast.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";
     if(-e $seqout){
        system("rm $seqout");
     }
     if(-e $seqhom){
        system("rm $seqhom");
     }
     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "$blastpath/bin/blastall -p blastp -i $seqpath/$NC_name -d $databasepath/$microbedb -o $seqout -I T -e $neval -m 8 -b $b \n";
     print JOBFILE "gzip $seqout \n";
     print JOBFILE "scp $seqout.gz $seqpath/\n";
     print JOBFILE "rm -rf $seqout.gz\n";
     print JOBFILE "rm -rf /scratch/phd/$NC_name\n"; 
   close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(3);
}
