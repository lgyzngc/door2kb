#!/usr/local/bin/perl -w
# Use a list of elements in one file, each item in one line
# search in the data file for the line with this element, and print out this line
# CAN CONTROL TO PRINT OUT THE ITEM IF NOT FOUND OR NOT

my $inputfile = $ARGV[0];
my $dataFile  = $ARGV[1];
my @seq;
my @data;

if (@ARGV <2) {

print "need inputfile datafile col printOpt\n";
die;
}
open (ANN3, "<$dataFile") or die "Can not open $dataFile....exit\n";

## store all promoter in the database in an array
while (<ANN3>) {
     if (defined ($_)) {
        chomp;
	#$_ =~ s/\s/ /g;
        push (@data, $_);
     }   
}

$leng = scalar(@data);

close (ANN3);

##Process the transcription factor name in the input file

open (ANN, "<$inputfile") or die "Can not open $inputfile....exit\n";
while(<ANN>)
{
    if (defined ($_)) {
	chomp;
        my $string = $_;  
        if (($string)) {
             ann1($string);
	 }else{
	     print "\n";
	 }
    }
}

close(ANN);

sub ann1 {
    my $item = shift;
    my $count = 0;
    my $found = 0;
    
    while ( ($count < $leng) && ($found ==0)) {
	my @l_arr = split (/\s/,$data[$count]);
	    if (($data[$count] =~ /$item/)) {
		my $lpos = $count +1;
		print "$data[$count]\n";
		++$found;
	    }
	    ++$count;             
                    
    }
    if ($found == 0) {
	print "$item NaN NaN\n";
    }
    
}
