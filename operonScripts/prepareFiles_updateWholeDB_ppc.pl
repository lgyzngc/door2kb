#!/usr/local/bin/perl -w

#this script will calculate features for the operon prediction
# it will need a faa file, ptt file and sequence file and a database of available microbe genomes for comparison
# it will do:
#1. blast search of the target genome against the microdatabase
#2. process the blast file
#3. print the directon from ptt file
#4. print adjacent pair from the directon
#5. print next pair from directon
#6. calculate IG between adj pair
#7. calculate genelength ratio between adj pair
#8. calculate the motif frequency in the promoter region from fna file
#9. calculate the phylo distance
#10 calculate the neighbor distance
#CHANGE E-VALUE OF BLAST TO 10-5 TO REDUCE THE BLAST FILE SIZE
my $inFile = $ARGV[0];          # This is the file contain NC number

my $microbedbpath= "/md3000/staff/maqin/operonPRED/microbe110109_faaDB";
my $scriptpath ="/md3000/staff/maqin/operonScripts";
my $path ="/md3000/staff/maqin";
my $microbeScratchPath = "/scratch/phd";

#blast
my $microbedb="microbe110109_faa";
my $blastpath = "/md3000/tools/i386/blast";
my $linx = "ppc64linux";
my $blast = "$scriptpath/runBlastppc_faa_e-2.pl";
my $blastout = $inFile .".faa.blast";
my $blastoutgz = $blastout .".gz";
my $processBlast = "$scriptpath/processBlastTab_faa.pl";
my $ncHom = $inFile .".faa.blast.hom";

#prepare gene ptt database
my $genomeList ="genomeList110109";
my $ncList = "NCList110109";
my $allgene ="allGene110109_ptt";
my $makeGI = "$scriptpath/addGI_genomefile_ffn.pl";
my $nallgene = $allgene ; ## the genome is in database .".$inFile";

#input file
my $outpath = $path . "/$inFile";
my $ptt = "$inFile" .".ptt";
my $faa = "$inFile" .".faa";
my $fna = "$inFile" .".fna";

#directon
my $printdirecton = "$scriptpath/print_directon_from_ptt_gi.pl";
my $ncdirecton = $inFile .".directon";

#adj pair &next pair ig, length ratio
my $printAdjPair = "$scriptpath/print_pair_in_operon.pl";
my $ncAdjPair = $inFile . ".adjPair";
my $printNextPair = "$scriptpath/print_nextGene_in_operon.pl";
my $ncNextPair = $inFile . ".nextPair";
my $runIG = "$scriptpath/print_IG_between_pair_with_gi.pl";
my $ncig = $inFile . ".ig";

my $runRatio = "$scriptpath/cal-length-ratio-between-gene-pairs-v2.pl";
my $ncRatio = $inFile .".rat";

#motif
my $motif = "$scriptpath/runCalMotif.pl";
my $mot = "upstreamMotif";
my $promLength = 100;
my $ncmotif = $inFile .".motif";

#phylo
my $phylo = "$scriptpath/runCalPhyPair3.pl";
my $phyeval = 1e-20;
my $genomeNum = 971;
my $ncphy = $inFile . ".phy";
my $ncphynext = $inFile . ".phynext";

#neighbor
my $neieval = 1e-20;
my $ncNum = 1822;
my $ncnei = $inFile . ".nei";
my $ncneinext = $inFile . ".neinext";

#score
my $score = "$scriptpath/runPrintOperon.pl";
my $ncscore = $inFile .".score";
my $scorethreshold = 3;
my $ncopr = $inFile .".opr";

my $scratchdir = "/scratch/phd/$inFile";
if (-e "$scratchdir"){
    system "rm $scratchdir/*\n";
}else {
    system "mkdir -p /scratch/phd/$inFile\n";
}
#system "cd $scratchdir\n";
my $nname = $microbedb . ".psd";
if (!(-e "$microbeScratchPath/$nname")){ ##$microbedb")){
   system "scp $microbedbpath/$microbedb\* $microbeScratchPath/\n";
}

runBlast();
##runAddGeneToList();a the genome is in the database
runDirecton();
runAdjpair();
runMotif();
runProcessBlast();
runPhylo();
runScore();
runCleanUp();

sub runCleanUp {
    my $newdir = $path ."/opPred1109Result/" . $inFile;
    system "rm $scratchdir/*\n";
    system "rmdir $scratchdir\n";
    system "mkdir $newdir\n";
    system "mv $path/$inFile/$ncopr $newdir/\n";
    system "mv $path/$inFile/$ncscore $newdir/\n";
    system "mv $path/$inFile/$blastoutgz $newdir/\n";
    system "rm $path/$inFile/*\n";
    system "rmdir $path/$inFile\n"; 
}
sub runScore {
    my $sdir = "$path/$inFile";
    system "perl $scriptpath/assignScore.pl $inFile $sdir/$ncscore $path/$inFile $ncphy $ncRatio $ncig $ncmotif $ncmotif $ncnei $ncneinext\n";
     system" perl $scriptpath\/print_operon_from_svm.pl $path/$inFile/$ncscore $scorethreshold $sdir/$ptt $scratchdir/$inFile.opr1\n";

     system " perl $scriptpath\/sort_operon.pl $scratchdir/$inFile.opr1 $sdir/$ncopr\n";

}

sub runPhylo {
    my $out1 = "$inFile.hom.phy.ext";
    my $out2 = "$inFile.hom.nei.ext";
    system "perl $scriptpath\/extract_phylo_wEcutoff_ffn.pl $path/$inFile/$ncHom $phyeval $genomeNum  $scratchdir/$out1\n";
     system "perl $scriptpath\/cal-phylo-dist-for-gene-pair.pl $scratchdir/$out1 $path/$inFile\/$ncAdjPair $path/$inFile/$ncphy\n";
     system "perl $scriptpath\/cal-phylo-dist-for-gene-pair.pl $scratchdir/$out1 $path/$inFile/$ncNextPair $path/$inFile/$ncphynext\n";

     system "perl $scriptpath\/extract_phylo_wEcutoff_ffn_printGI.pl $path/$inFile/$ncHom $neieval $ncNum  $scratchdir/$out2\n";
     system "perl $scriptpath\/calculatePairwiseHomologAdjacent_ffn.pl $scratchdir/$out2 $path/$inFile/$ncAdjPair $path/$inFile/$ncnei\n";
     system "perl $scriptpath\/calculatePairwiseHomologAdjacent_ffn.pl $scratchdir/$out2 $path/$inFile/$ncNextPair $path/$inFile/$ncneinext\n";
}

sub runMotif {
    my $sdir = "$path/$inFile";
    system "perl $scriptpath\/cutUPstreamGenome.pl $sdir/$ptt $sdir\/$fna $promLength \>$scratchdir\/$inFile.prom\n"; ##cut upstream of whole genome
    system "perl $scriptpath\/print_downstreamGI.pl $sdir/$ncig \> $scratchdir\/$inFile.downstream\n"; ## print the downstream gene in the gene pair
    system "perl $scriptpath\/calculateMotifFrequency.pl $scratchdir\/$inFile.prom $scriptpath/$mot \>$scratchdir\/$inFile.prom.freq\n"; ## find the motif frequency
    system"perl $scriptpath\/printSelectMotif.pl $scratchdir\/$inFile.downstream $scratchdir\/$inFile.prom.freq \> $scratchdir/$ncmotif\n"; ## print the promoter of the downstream gene
    system "scp $scratchdir/$ncmotif $sdir/\n";
}

sub runProcessBlast {
    
   system "perl $processBlast $path/$inFile/$blastoutgz $scriptpath/$nallgene $path/$inFile/$ncHom\n"; ##change the nallgene path
}

sub runAdjpair {
    if (-e "$path/$inFile/$ncdirecton") {
	system "perl $printAdjPair $path/$inFile/$ncdirecton $path/$inFile/$ncAdjPair\n";
	system "perl $printNextPair $path/$inFile/$ncdirecton $path/$inFile/$ncNextPair\n";
	system "perl $runIG $path/$inFile/$ptt $path/$inFile/$ncAdjPair $path/$inFile/$ncig\n";
	system "perl $runRatio $path/$inFile/$ptt $path/$inFile/$ncAdjPair $path/$inFile/$ncRatio\n";
	
    }
}

sub runDirecton {
    system "perl $printdirecton $path/$inFile/$ptt $path/$inFile/$ncdirecton\n";
}

sub runAddGeneToList {
    my $ngenomeList = $genomeList . ".$inFile";
    my $nncList = $ncList .".$inFile";
    
    system "perl $makeGI $path/$inFile/$ptt $scriptpath/$genomeList $scriptpath/$ncList $path/$inFile/$nallgene $path/$inFile/$ngenomeList $path/$inFile/$nncList\n";
    system "cat $scriptpath/$allgene >> $path/$inFile/$nallgene \n"; 

}

sub runBlast{
   my $neval = 1e-5; ##changed 103109
   my $b =5000;
   system "$blastpath/bin/blastall -p blastp -i $path/$inFile/$faa -d $microbeScratchPath/$microbedb -o $scratchdir/$blastout -I T -e $neval -m 8 -b $b \n";
   system "gzip $scratchdir/$blastout \n";
   system "scp $scratchdir/$blastoutgz $path/$inFile/\n";
}
