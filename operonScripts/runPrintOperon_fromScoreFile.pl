#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = $ARGV[0]; #"ppc64linux";
   my $path = $ARGV[1]; #prediction result path;
   my $scriptpath = $ARGV[2]; #"operons"; 
   my $inFile = $ARGV[3];          # NC number
   my $scorefile = $ARGV[4]; #score file with absolute path
   my $val = $ARGV[5]; #3; ## threshold for the cutoff score
   my $ptt = $ARGV[6];#ptt file with absolute path
   my $oprout = $ARGV[7];
   ####################################################################
  printjobfile();


sub printjobfile{
     
     my $seqout="/scratch/phd/$oprout";
     my $out1 = "/scratch/phd/$inFile.op";

     my $cmdfile="$path/$inFile.score.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE " perl $scriptpath\/print_operon_from_svm.pl $scorefile $val $ptt $out1\n";

     print JOBFILE " perl $scriptpath\/sort_operon.pl $out1 $seqout\n";

     print JOBFILE "scp $seqout $path\/$oprout\n";
     print JOBFILE "rm -rf \/scratch\/phd\/$inFile*\n";

     close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
