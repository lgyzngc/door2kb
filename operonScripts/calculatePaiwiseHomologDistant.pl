#!/usr/bin/perl
use POSIX;
# use phyloTable contain the gene position in each genome
# map the gene to its homolog in another genome with a try to maximize the conservedness of the region
## Need to do each NC separate because the genes will be order by the position in the genome
my $phy_file = $ARGV[0];
my @genomes;
my @genes;
my %cur_genome_phy;
my %gene_phy;
my $nan = "--"; ## value to print out when not found
my @oriOP;
my $numGenome =0;
my @temPos;
my @curPos;
my @curGenome;
my @curGenome2;
my @result;

if (@ARGV <1) {
    print "Usage : phylo_file\n";
    die;
}
if (!defined $print_opt){
    $print_opt = 0;
}

processPhyfile();

for (my $i = 0; $i < @genes; ++$i) {
    getCurGenome($i);
    for (my $j = $i+1; $j < @genes; ++$j) {
	getCurGenome2($j);
	my $val = runList();
	my $line = "$i $j $genes[$i] $genes[$j] " .$val;
	push (@result,$line);
	print "$line\n";
    }
}

sub getCurGenome{
    my $col = shift();
    @curGenome =();
   
    my $name = $genes[$col];
    @curGenome = split(/\s/,$gene_phy{$name});
}

sub getCurGenome2{
    my $col = shift();
    @curGenome2 =();
     my $name = $genes[$col];
    @curGenome2 = split(/\s/,$gene_phy{$name});
}
################
sub runList {
    my @larr;
    for (my $i = 1; $i < @curGenome; ++$i) {
	my $curHom = $curGenome[$i];
	my $curHom2 = $curGenome2[$i];
	if (($curHom ne "0")&&($curHom =~ m/,/)) {
	    my @minPrev;
	    my @curPos2;

	    @temPos=();
	    getPos($curHom);
	    @curPos = @temPos;
	    #print "curHom1 $curHom curHom2 $curHom2\n";
	    if (($curHom2 ne "0")&&($curHom2 =~ m/,/)) {
		@temPos=();
		getPos($curHom2);
                @curPos2 = @temPos;
		#print "curPos2 @curPos2\n";
		@temPos=();
		getDist(@curPos2);
		@minPrev = @temPos;
	    }else {
		$curPos2[0] = $curHom2;
	    }
	    if ((@minPrev >2)) { ## no homolog in the adjacent genes
		#print "result $minPrev[2]\n";
	     
		push(@larr,$minPrev[2]);
	    }
	}
	
    }
    if (scalar(@larr) == 0) {
	return $nan;
    }else {
	my @la2 = sort{$a<=>$b}@larr;
	my $leng =floor((scalar(@la2))/2);
	my $val = scalar(@la2);
	if (scalar(@la2) > 1){
	    $val = $val ."_" .$la2[1] ."_" .$la2[$leng];
	}else {
	    $val = $val ."_" .$la2[0] ."_" .$la2[$leng];
	}
	return $val;
    }
  
 
}##end sub runList

sub getDist {
    my $mindis = 10000;
    my $minInd1;
    my $minInd2;
    my @lar =@_;
  # print "inside getDist @lar @curPos\n"; 
    for (my $k = 0; $k < @lar; ++$k) {
	#print "k $k\n";
	for (my $q = 0; $q < @curPos; ++$q) {
	    #print "q $q\n";
	    if ($lar[$k] == $curPos[$q])  { ## same NC
		if (($k == 0) &&($q ==0)){
		    $minInd1 = $k+1;
		    $minInd2 = $q+1;
		    $mindis = abs($lar[($k+1)] - $curPos[($q+1)]);
		}else {
		    my $lval = abs($lar[($k+1)] - $curPos[($q+1)]);
		    if ($lval < $mindis) {
			$minInd1 = $k+1;
			$minInd2 = $q+1;
			$mindis = $lval;
		    }
		}
		
	    }
	    $q = $q+1;
	}
	$k = $k+1;
    }
    push(@temPos,$minInd2, $minInd1,$mindis);

}

sub getPos {
    my $lstr= shift();
    my @lar = split(/_/,$lstr); ## _ as delimitor for gene
    for (my $j = 0; $j <@lar; ++$j) {
	my @llar = split (/,/,$lar[$j]);
	push (@temPos,$llar[1], $llar[4]);
    }
    
}
##################################
sub checkEvalue {
    my $l_arr = shift;
    if ($l_arr =~ /^e/){
	$l_arr = "1" .$l_arr;
    }
    if ($evalue =~ /^e/) {
	$evalue = "1" . $evalue;
    }
    if ($l_arr <= $evalue) {
	return 1;
    }else {
	return 0;
    }
}


###################
##PROCESS PHYLO FILE
sub processPhyfile {
    if ($phy_file =~ /\.gz/){
	open (INFILE, "gunzip -c $phy_file |" ); ##OPEN FROM A ZIP FILE
    }elsif (-e "$phy_file") {
	open INFILE, "$phy_file";
    }else {
	die ("Cannot open phy_file  $phy_file\n");
    }
    while (<INFILE>){
	my $str = $_;
	chomp($str);
	my @l_arr = split (/\s/,$str);	
	my @arr1 = split(/\,/,$l_arr[0]);
	$arr1[0] =~ s/[(,)]//g;
        push(@genes,$arr1[0]);
	$gene_phy{$arr1[0]} = $str;
	if ($numGenome == 0) {
	    $numGenome = scalar(@l_arr) -1 ;
	}elsif ($numGenome != (scalar(@l_arr) -1)){
	    print "ERR: $phy_file: not equal # of column gene: $arr1[0]\n";
	}    
    }
    close (INFILE);
}

