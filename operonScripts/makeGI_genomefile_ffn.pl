#!/usr/local/bin/perl -w

my $file = $ARGV[0]; #ptt file with genome name and NC number
my $genomelist = $ARGV[1];
my $nclist = $ARGV[2];
my $out = $ARGV[3];
my %genome;
my %nc;

open (ANN1,"<$genomelist") or die "can not open $genomelist\n";
while (<ANN1>) {
  chomp;
  my @lar = split (/\s/,$_);
  $lar[1] =~ s/\s*//g;
  $genome{$lar[1]} = $lar[0];
}
close(ANN1);

open (ANN1,"<$nclist") or die "can not open $nclist\n";
while (<ANN1>) {
   chomp;
   my @lar = split (/\s/,$_);
   $lar[1] =~ s/\s*//g;
   $nc{$lar[1]} = $_;

}
close (ANN1);
my @lar = split (/\//,$file);
my @lar2 = split (/\./,$lar[-1]);
my $ncName = $lar2[0];
$ncName =~ s/\.\w*//;
my $found = 0;
my $ncNum =0;
my $genomeName;
my $genomeNum =0;
if (defined ($nc{$ncName}) && ($nc{$ncName} ne "")) {
   my @lval = split (/\s/,$nc{$ncName});
   $ncNum = $lval[0];
   $genomeName = $lval[2];
   $genomeNum = $genome{$genomeName}; 

}

open (ANN, "<$file") or die "can not open $file\n";
my $count = 0;
while ($count <3) {
my $line = <ANN>; ##remove top 3 lines
++$count;
}
my $pos = 1;
open (OUT, ">$out") or die "can not open $out\n";
while(<ANN>)
{
   chomp;
   my $str = $_;
   my @larr = split (/\t/,$str);
   my ($loc1,$loc2) = split(/\.+/,$larr[0]);
   my $loc;
   if ($larr[1] eq "+") {
       $loc = $loc1 ."-" .$loc2;
   }else {
       $loc = "c" . $loc2 ."-" .$loc1;
   }
   print OUT "$loc $ncName $larr[3] $ncNum $genomeNum $larr[1] $pos\n";
   ++$pos;

}
close(ANN); 
close (OUT);
