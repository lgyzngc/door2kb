#!/usr/local/bin/perl -w

#this script will calculate features for the operon prediction
# it will need a faa file, ptt file and sequence file and a database of available microbe genomes for comparison
# it will do:
#1. blast search of the target genome against the microdatabase
#2. process the blast file
#3. print the directon from ptt file
#4. print adjacent pair from the directon
#5. print next pair from directon
#6. calculate IG between adj pair
#7. calculate genelength ratio between adj pair
#8. calculate the motif frequency in the promoter region from fna file
#9. calculate the phylo distance
#10 calculate the neighbor distance

my $inFile = $ARGV[0];          # This is the file contain NC number

my $microbedbpath= "/md3000/staff/phd/microbe011409_faaDB";
my $scriptpath ="/md3000/staff/phd/operonScripts";
my $path ="/md3000/staff/phd";

#blast
my $microbedb="microbe011409_faa";
my $blastpath = "\/gpfs1\/glinux\/ncbi";
my $linx = "glinux";
my $blast = "$scriptpath/runBlastppc_faa_e-2.pl";
my $blastout = $inFile .".faa.blast.gz";
my $processBlast = "$scriptpath/processBlastTab_faa.pl";
my $ncHom = $inFile .".faa.blast.hom";

#prepare gene ptt database
my $genomeList ="genomeList011409";
my $ncList = "NCList011409";
my $allgene ="allGene011409_ptt";
my $makeGI = "$scriptpath/addGI_genomefile_ffn.pl";
my $nallgene = $allgene .".$inFile";

#input file
my $outpath = $path . "/$inFile";
my $ptt = "$inFile" .".ptt";
my $faa = "$inFile" .".faa";
my $fna = "$inFile" .".fna";

#directon
my $printdirecton = "$scriptpath/print_directon_from_ptt_gi.pl";
my $ncdirecton = $inFile .".directon";

#adj pair &next pair ig, length ratio
my $printAdjPair = "$scriptpath/print_pair_in_operon.pl";
my $ncAdjPair = $inFile . ".adjPair";
my $printNextPair = "$scriptpath/print_nextGene_in_operon.pl";
my $ncNextPair = $inFile . ".nextPair";
my $runIG = "$scriptpath/print_IG_between_pair_with_gi.pl";
my $ncig = $inFile . ".ig";

my $runRatio = "$scriptpath/cal-length-ratio-between-gene-pairs-v2.pl";
my $ncRatio = $inFile .".rat";

#motif
my $motif = "$scriptpath/runCalMotif.pl";
my $mot = "upstreamMotif";
my $promLength = 100;
my $ncmotif = $inFile .".motif";

#phylo
my $phylo = "$scriptpath/runCalPhyPair3.pl";
my $phyeval = 1e-20;
my $genomeNum = 675;
my $ncphy = $inFile . ".phy";
my $ncphynext = $inFile . ".phynext";

#neighbor
my $neieval = 1e-20;
my $ncNum = 1565;
my $ncnei = $inFile . ".nei";
my $ncneinext = $inFile . ".neinext";

#score
my $score = "$scriptpath/runPrintOperon.pl";
my $ncscore = $inFile .".score";
my $scorethreshold = 3;
my $ncopr = $inFile .".opr";

runProcessBlast();
if (!(-e "$path/$inFile/$ncHom")){
   sleep (1);
}
runPhylo();
sleep(3);
if (!(-e "$path/$inFile/$ncneinext")|| !(-e "$path/$inFile/$ncnei") || !(-e "$path/$inFile/$ncphy") || !(-e "$path/$inFile/$ncRatio") || !(-e "$path/$inFile/$ncig")){
   sleep (3);
}
runScore();

sub runScore {
    system "perl $score $linx $path $scriptpath $inFile $ncscore $ncphy $ncRatio $ncig $ncmotif $ncmotif $ncnei $ncneinext $scorethreshold $ptt $ncopr\n";

}

sub runPhylo {
    system "perl $phylo $linx $path/$inFile $phyeval $genomeNum $inFile $ncAdjPair $ncHom $ncphy $scriptpath $neieval $ncNum $ncnei $ncNextPair $ncphynext $ncneinext";

}
sub runMotif {
    system "perl $motif $linx $path/$inFile $scriptpath/$mot $inFile $scriptpath $promLength $ncmotif\n";
}

sub runProcessBlast {
   system "perl $processBlast $path/$inFile/$blastout $path/$inFile/$nallgene $path/$inFile/$ncHom\n";
}

sub runAdjpair {
    if (-e "$path/$inFile/$ncdirecton") {
	system "perl $printAdjPair $path/$inFile/$ncdirecton $path/$inFile/$ncAdjPair\n";
	system "perl $printNextPair $path/$inFile/$ncdirecton $path/$inFile/$ncNextPair\n";
	system "perl $runIG $path/$inFile/$ptt $path/$inFile/$ncAdjPair $path/$inFile/$ncig\n";
	system "perl $runRatio $path/$inFile/$ptt $path/$inFile/$ncAdjPair $path/$inFile/$ncRatio\n";
	
    }
}

sub runDirecton {
    system "perl $printdirecton $path/$inFile/$ptt $path/$inFile/$ncdirecton\n";
}

sub runAddGeneToList {
    my $ngenomeList = $genomeList . ".$inFile";
    my $nncList = $ncList .".$inFile";
    
    system "perl $makeGI $path/$inFile/$ptt $scriptpath/$genomeList $scriptpath/$ncList $path/$inFile/$nallgene $path/$inFile/$ngenomeList $path/$inFile/$nncList\n";
    system "cat $scriptpath/$allgene >> $path/$inFile/$nallgene \n"; 

}

sub runBlast{
   my $name = $inFile;
   my $database = "$microbedbpath/$microbedb";
   my $seqpath = "$path/$inFile";
   my $eval = 1e-2;
   my $b =5000;
   system "perl $blast $blastpath $linx $microbedbpath $seqpath $eval $b $name $microbedb\n";


}
