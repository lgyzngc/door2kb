#!/usr/local/bin/perl -w

#print commands to run large number of files
   my $blastpath = "\/gpfs1\/glinux\/ncbi";
   my $linx = "glinux";
   my $database = "\/md3000/staff/phd/operonPreJan09\/microbe011409_faaDB";
   my $seqpath = "\/md3000/staff/phd/operonPreJan09\/faa";
   my $eval = 1e-2;
   my $b =5000;
   my $name = $ARGV[0];          #sequence file
   ####################################################################
   my $dir = $name;
   system ("mkdir $dir");
   my $seqName = "$name.faa";
  system "pwd \> tmp";
  open (TMP, "<tmp");
  my $absdir = <TMP>;
  chomp ($absdir);
  close(TMP);
  system "rm tmp\n";
  printjobfile($seqName, $dir,$absdir);


sub printjobfile{
     my $NC_name = shift();
     my $ldir = shift();
     my $labsdir = shift();
     my $lnr = "\/scratch\/phd\/microbe011409_faa";
     my $seqout="/scratch/phd/$NC_name.blastMicrobe014409faa";
     my $seqhom="$labsdir/$ldir/$NC_name.blastMicrobe011409faa";
     my $cmdfile="$labsdir\/$ldir\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";
     if(-e $seqout){
        system("rm $seqout");
     }
     if(-e $seqhom){
        system("rm $seqhom");
     }
     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "if  [ ! -e $lnr.nhr ] ; then scp $database/* /scratch/phd/; fi\n";
     print JOBFILE "scp $seqpath/$NC_name /scratch/phd/ \n";
     print JOBFILE "$blastpath/bin/blastall -p blastp -i /scratch/phd/$NC_name -d $lnr -o $seqout -I T -e $eval -m 8 -b $b \n";
     print JOBFILE "gzip $seqout \n";
     print JOBFILE "scp $seqout.gz $labsdir/$ldir/\n";
     print JOBFILE "rm -rf $seqout.gz\n";
     print JOBFILE "rm -rf /scratch/phd/$NC_name\n"; 
 
   close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     sleep(3);
}
