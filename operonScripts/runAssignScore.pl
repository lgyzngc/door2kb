#!/usr/local/bin/perl -w

#call generateFeatureFile_withNextPair.pl, then call assign_score.pl

   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $subdir = "genePairNextScore";
   my $name = $ARGV[0];
   ####################################################################
  printjobfile($name);


sub printjobfile{
     my $NC_name = shift();
     

     my $seqout="/scratch/phd/$NC_name.scoreN";

     my $cmdfile="$dir\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE "perl $dir\/assignScore.pl $NC_name $seqout\n";
     print JOBFILE "scp $seqout $dir\/$subdir\/\n";
     print JOBFILE "rm -rf \/scratch\/phd\/$NC_name*\n";

     close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
