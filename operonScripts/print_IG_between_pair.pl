#!/usr/local/bin/perl -w

# This program is used to print the intergenic distance from ptt file
# and a list of gene pair file
# with the pair in the same direction, print the IG between them
# assuming no intervening genes
# with the divergent pair, the IG is between the starts
# with the convergent pair, print the space between 2 ends
# also indicate if the pair is C(convergent), D (divergent) or F/R (unidirection)
# WILL PRINT IG less than 0 IF PAIR IS OVERLAPED

my @ptt;
my $dir1;
my $dir2;
my $pair_dir;
my $starts;
my $startl;
my $ends;
my $endl;
my $orf1;
my $orf2;
my $inFile = $ARGV[0];          # This is the ptt file
my $search = $ARGV[1]; # file with pairs of neighbors
my $err = $ARGV[2];
my @not_found;
my @ig;
   ####################################################################
if (@ARGV < 2) {
    print "usage: ptt-file pair-file err \n";
    die;
}
open (ANN, "<$inFile") or die "Can not open $inFile ... exit\n";
if (!(defined $err)) {
    $err = "print_ig_err";
}
open (ERR, ">$err") or die "Can not open $err ... exit\n";

my $str = <ANN>; ##first line
$str = <ANN>;  ##second line
$str = <ANN>;  ##third line

while (<ANN>) {
    $str = $_; ##fourth line and on
    chomp ($str);   
    $str =~ s/^\s+//; ##REMOVE SPACE AT THE START   
    $str =~ s/\Z\s+//; ##REMOVE SPACE AT THE END   
    my @arr = split (/\t/, $str,7);
    $arr[0] =~ s/\.+/ /;
    $arr[0] =~ s/^\s+//;
    $arr[1] =~ s/\s+//;
    $arr[5] =~ s/\s+//;
    #print "arr5 $arr[5]\n";
    my @l_arr = split (/\s/,$arr[0]);
    $arr[0] = $l_arr[1];
    unshift (@arr, $l_arr[0]);
    push (@ptt, $arr[6]);
    my $name = $arr[6];
    @$name = ($arr[0], $arr[1], $arr[2], $arr[6]);   
    
}
close(ANN);

### start searching
open (ANN, "<$search") or die "Can not open $search ... exit\n";

OUTER: while (<ANN>) {
    my $ind1;
    my $ind2;
    my $found_ind = 0;
    my $line = $_;
    chomp ($line);
    my @arr = split (/\s/,$line);
    $orf1 = $arr[0];
    $orf1 =~ s/\s//g;
    $orf2 = $arr[1];
    $orf2 =~ s/\s//g;
    for (my $i = 0; ($i< @ptt) && ($found_ind <2); ++$i) {
	if ($orf1 eq $ptt[$i]) {
	    $ind1 = $i;
	    ++$found_ind;
	}elsif ($orf2 eq $ptt[$i]) {
	    $ind2 = $i;
	    ++$found_ind; 
	}
    }
    if ($found_ind <2) {
	push (@not_found, $line);
	print ERR "not found in ptt $line\n";
	next OUTER;
    }else {
	if ($ind1 > $ind2) {
	    ($ind1,$ind2) = ($ind2,$ind1);
	}elsif ($ind1 == $ind2) {
	    push (@not_found, $line);
	    print "not found because same bNumber $line \n";
	    next OUTER;
	}
	$orf1 = $ptt[$ind1];
	$orf2 = $ptt[$ind2];
	$dir1 = $$orf1[2];
	$dir2 = $$orf2[2];
	$starts = $$orf1[0];
	$startl = $$orf2[0];
	$ends = $$orf1[1];
	$endl = $$orf2[1];
	if ($dir1 eq $dir2) {
	    if ($dir1 eq "+"){
		$pair_dir = "F";		
	    }elsif ($dir1 eq "-") {
		$pair_dir = "R";
	    }else {
		push (@not_found, $line);
		print ERR "not found because of unidirection, $dir1 $dir2\n";
		next OUTER;
	    }	    
	}elsif ($dir1 eq "+"){
	    $pair_dir = "C";
	}elsif ($dir2 eq "+") {
	    $pair_dir = "D";
	}else {
	    push (@not_found, $line);
	    print ERR "not found because of pair_dir, $dir1 $dir2\n";
	    next OUTER;
	}
	#if ($startl < $ends) {
	    #$pair_dir = "O";
	    #push (@not_found, $line);
	    #print ERR "not found because of overlap $line\n";
	    #next OUTER;
	#}
	my $leng = $startl-$ends;
	$n_line = $line . " " . $pair_dir . " " . $leng;
	print "$n_line\n";
    }
}
close (ANN);
