#!/usr/local/bin/perl -w

# This program is used to print directon from ptt file
# Directon: stretch of genes transcribed in the same direction
# without intervening genes in the other strand
# THIS SCRIPT CHANGE SIGNIFICANTLY COMPARE TO THE PRINT_DIRECTON.PL
# BECAUSE THE FIRST DIRECTON WILL BE EXAMINED IF IT CONTAINS THE LAST
# GENE IN THE PTT FILE

my @directon;
my $curr_dir;
my $new_dir;
my @ptt;
my $linear = 0;
my $inFile = $ARGV[0];          # This is the first file
my $out = $ARGV[1];   
   ####################################################################
open (ANN, "<$inFile") or die "Can not open $inFile ... exit\n";
open (OUT, ">$out") or die "Can not open $out ... exit\n";
my $str = <ANN>; ##first line
$str = lc($str);
if ($str =~ /linear/) { ##LINEAR CHROMOSOME
    $str = <ANN>;  ##second line
    $str = <ANN>;  ##third line

    while (<ANN>) {
	$str = $_; ##fourth line and on
	chomp ($str);
	$str =~ s/\t/ /g;   
	$str =~ s/\s+/ /g;    
	$str =~ s/^\s//; ##REMOVE SPACE AT THE START   
	$str =~ s/\Z\s//; ##REMOVE SPACE AT THE END   
	my @arr = split (/\s/, $str);
    
	if (!defined ($curr_dir)) {
	    push (@directon, $arr[3]);
	    $curr_dir = $arr[1];
	}else {
	    $new_dir = $arr[1];
	    if ($new_dir eq $curr_dir) {
		push (@directon, $arr[3]);
	    }else {
		for (my $i= 0; $i <@directon; ++$i) {
		    print OUT "$directon[$i]\t";
		}
		print OUT "\n";
		@directon= ();
		push (@directon, $arr[3]);
		$curr_dir = $arr[1];
	    }
	}	 
    }
    close(ANN);
    for (my $i= 0; $i <@directon; ++$i) {
	print OUT "$directon[$i]\t";
    }
    print OUT "\n";
}else {
    $str = <ANN>;  ##second line
    $str = <ANN>;  ##third line

    while (<ANN>) {
	$str = $_; ##fourth line and on
	chomp ($str);
	$str =~ s/\t/ /g;   
	$str =~ s/\s+/ /g;    
	$str =~ s/^\s//; ##REMOVE SPACE AT THE START   
	$str =~ s/\Z\s//; ##REMOVE SPACE AT THE END 
	push (@ptt, $str);
    } 
    close (ANN);
    
## CHECK IF THE LAST GENE ARE THE SAME DIRECTION WITH THE FIRST GENE
## if so, move the last item to the start of ptt array

    my @arr1 = split (/\s/,$ptt[0]);
    my $stop = 0;
    my $count = @ptt;
    while (($stop == 0) && ($count > 0)) {
	my @arr2 = split (/\s/,$ptt[-1]);
   
	if ($arr1[1] eq $arr2[1]){
	    my $ln1 = pop (@ptt);
	    unshift (@ptt,$ln1);
	    --$count;
	}else {
	    ++$stop;
	}
    }
    for (my $i = 0; $i < @ptt; ++$i) {
	my @arr = split (/\s/, $ptt[$i]);
    
	if (!defined ($curr_dir)) {
	    push (@directon, $arr[3]);
	    $curr_dir = $arr[1];
	}else {
	    $new_dir = $arr[1];
	    if ($new_dir eq $curr_dir) {
		push (@directon, $arr[3]);
	    }else {
		for (my $i= 0; $i <@directon; ++$i) {
		    print OUT "$directon[$i]\t";
		}
		print OUT "\n";
		@directon= ();
		push (@directon, $arr[3]);
		$curr_dir = $arr[1];
	    }
	}	 
    }
    
    for (my $i= 0; $i <@directon; ++$i) {
	print OUT "$directon[$i]\t";
    }
    print OUT "\n";



}

close(OUT);
