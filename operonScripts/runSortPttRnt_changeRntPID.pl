#!/usr/local/bin/perl -w

## cat ptt and rnt file, then run sort file

#print commands to run large number of files
   my $linx = "ppc64linux";
   my $dir = "\/md3000\/staff\/phd\/opPred1109";
   my $pttdir = "\/md3000\/staff\/phd\/ptt";
   my $program = "sort_ptt_rnt_file_changeRntPID.pl";


   my $inFile = $ARGV[0];          # This is the file name without ptt or rnt
	####################################################################
   my $ptt = $inFile . ".ptt";
   my $rnt = $inFile . ".rnt";
   my $out = $inFile . ".ptt_rnt.sorted";

     my $cmdfile="$dir\/$inFile.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE "if  [ -e $pttdir/$rnt ] ; then perl $dir\/$program $pttdir\/$ptt $pttdir\/$rnt $pttdir\/$out\n";
     print JOBFILE  "rm $pttdir\/$ptt\n";
     print JOBFILE "rm $pttdir\/$rnt; fi\n";
     close JOBFILE;

     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.5);


