#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = "ppc64linux";
   my $dir = "\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $pttdir = "pttMay08";
   my $subdir = "genomeIG";
   my $genePairdir = "genePairs";
   my $name = $ARGV[0];          #sequence file
   ####################################################################
  printjobfile($name);


sub printjobfile{
     my $NC_name = shift();
     my $ptt = $NC_name . ".ptt_rnt.sorted";
     my $NC_pair = $NC_name .".adjPair";
     my $lnr = "\/scratch\/phd\/$ptt";
     my $pair = $NC_name .".adjPair";
     my $seqout="/scratch/phd/$NC_name.IG";
     my $cmdfile="$dir\/$NC_name.sh";
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";
     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";
     print JOBFILE "scp $dir\/$pttdir\/$ptt /scratch/phd/ \n";
     print JOBFILE "perl $dir\/print_IG_between_pair_with_gi.pl $lnr $dir\/genePairs\/$pair \>$seqout\n";
     print JOBFILE "scp $seqout $dir\/$subdir\/\n";
     print JOBFILE "rm -rf $seqout\n";
     print JOBFILE "rm -rf /scratch/phd/$NC_name.*\n"; 
     #print JOBFILE "rm -rf /scratch/phd/$NC_pair\n";
    close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.5);
}
