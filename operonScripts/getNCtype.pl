#!/usr/local/bin/perl -w

my $inputfile = $ARGV[0];
my @NC;
my @genome;

open (ANN, "<$inputfile") or die "Can not open $inputfile....exit\n";

while(<ANN>)
{
   chomp;
   my $string = $_;

   if (!($string eq "") &&($string =~ /NC_00/)) {
       my @arr1 = split(/\s/,$string);
       my @arr = split(/\./,$arr1[1]);
       push (@NC,$arr[0]);
       print "$arr[0] ";
   }elsif (!($string eq "")) {
       if (($string =~ /genome/)) { #|| () {
	   push(@genome,"genome");
	   print "genome\n";
       }elsif ( $string =~ /chromosome/) {
	   push(@genome,"chromosome");
	   print "chromosome\n";
       }elsif ($string =~ /plasmid/) {
	   push(@genome,"plasmid");
	   print "plasmid\n";
       }
   }
}

close(ANN);

