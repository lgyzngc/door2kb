#!/usr/local/bin/perl -w

#this script will generate feature file for operon prediction using matlab predictor
# the feature file will include the gene ID (2 genes) then the phylogenetic hamming distance, then 
# the phy-ent, length ratio, IG, mot1(threeleter, #13), mot2 (five letter, #1024), neighbor score (simple count);


   my $inFile = $ARGV[0];          # This is the file contain NC number
   my $scorefile = $ARGV[1];
   my $path = $ARGV[2]; #"\/gpfs0\/home\/staff\/phd\/operonPreMay08\/";  
   my $phytail = $ARGV[3]; #".phyDist";
   my $lengtail = $ARGV[4]; #".lengRat";
   my $igtail = $ARGV[5]; #".IG";
   my $mot1tail = $ARGV[6];#".motif";
   my $mot2tail = $ARGV[7];#".motif";
   my $neitail = $ARGV[8];#".neiDist";
   my $neitail2 = $ARGV[9];#".neiNext";

   my @genomename ;
   $genomename[0] = $inFile;
   my @phy;
my @w = (0, 0.0019, 0, 0.0163, 0.3045, 0.0791, -0.0078, -0.0078);
   ####################################################################
my $mot1col = 2;
my $mot2col = 3; 
open (OUT, ">$scorefile");

for (my $i = 0; $i < @genomename; ++$i) {
    @phy =();
    my $printfile = 0;
    my $val = addPhy($genomename[$i]);
    if ($val == 1) {
	$val = addLeng($genomename[$i]);
	if ($val == 1) {
	    $val = addIG($genomename[$i]);
	    if ($val ==1 ) {
		$val = addMot1($genomename[$i]);
		if ($val ==1) {
		    $val = addMot2($genomename[$i]);
		    if($val == 1) {
			$val = addNei($genomename[$i]);
			if ($val ==1) {
			     $val = addNei2($genomename[$i]);
			     if ($val == 0) {
				 print OUT "error in nei2 $genomename[$i]\n";
			     }
			}else{
				 print OUT "error in nei $genomename[$i]\n";
				 ++$printfile;
			}
		    }else{
			print OUT "error in Mot2 $genomename[$i]\n";
			++$printfile;
		    }
		}else{
		    print OUT "error in Mot1 $genomename[$i]\n";
		    ++$printfile;
		}
	    }else{
		print OUT "error in IG $genomename[$i]\n";
		++$printfile;
	    }
	    
	}else{
	    print OUT "error in leng $genomename[$i]\n";
	    ++$printfile;
	}
	
    }else{
	print OUT "error in phy $genomename[$i]\n";
	++$printfile;
    }

    if ($printfile ==0){
	for (my $j = 0; $j <@phy; ++$j) {
	    my @arr = split(/\s/,$phy[$j]);
	    my $lw = 0;
	    for (my $k = 2; $k <@arr; ++$k) {
		#print "\nlw=$lw,w[($k-2)]=$w[($k-2)],arr=$arr[$k]\n";
		next if ($arr[$k]=~/NaN/) ;
		$lw += $w[($k-2)]*$arr[$k];
	    }
	    print "$arr[0]\n$arr[1]\n$lw\n";
	    print OUT "$arr[0] $arr[1] $lw\n";
	}
	
    }
   
}
 close(OUT);
sub addPhy {
    my $lname = shift;
    my $file = "$path/$phytail";
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@phy,$_);
	}
	print "$phy[0]\n";
	return 1;
    } else {
	return 0;
    }
}
sub addLeng {
    my $lname = shift;
    my $file = "$path/$lengtail";
    my @leng ;
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print OUT "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/) && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[2];
	    }else {
		print OUT "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print OUT "$file not the same order\n";
	    return 0;
	}else{
	#   print "addLeng:$phy[0]\n";
	    return 1;
	}
    } else {
	return 0;
    }

}

sub addIG{
    my $lname = shift;
    my $file = "$path/$igtail";
    my @leng ;
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print OUT "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/) && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[3];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print OUT "$file not the same order\n";
	    return 0;
	}else{
	#   print "addIG:$phy[0]\n";
	    return 1;
	}
    } else {
	return 0;
    }
}

sub addMot1 {
    my $lname = shift;
    my $file = "$path/$mot1tail";
    my @leng ;
    if (-e $file) {
	if ($file =~ /gz/){
	    open(ANN,"gunzip -c $file |" );
	}else{
	    open (ANN, "$file");
	}

	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print OUT "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/)) { ## && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[$mot1col];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print OUT "$file not the same order\n";
	    return 0;
	}else{
	#	print "addMot1:$phy[0]\n";
	    return 1;
	}
    } else {
	print OUT "file not found $file\n";
	return 0;
    }
}


sub addMot2{
     my $lname = shift;
    my $file = "$path/$mot2tail";
    my @leng ;
    if (-e $file) {
	if ($file =~ /gz/){
	    open(ANN,"gunzip -c $file |" );
	}else{
	    open (ANN, "$file");
	}

	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print OUT "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/)) { ## && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[$mot2col];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print OUT "$file not the same order\n";
	    return 0;
	}else{
	 #   print "addMot2:$phy[0]\n";
	    return 1;
	}
    } else {
	print OUT "file not found $file\n";
	return 0;
    }
}

sub addNei{
     my $lname = shift;
    my $file = "$path/$neitail";
    my @leng ;
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print OUT "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/) && ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[2];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print OUT "$file not the same order\n";
	    return 0;
	}else{
	  #  print "addNei:$phy[0]\n";
	    return 1;
	}
    } else {
	return 0;
    }
}
sub addNei2{
     my $lname = shift;
    my $file = "$path/$neitail2";
    my @leng ;
    if (-e $file) {
	open (ANN, "<$file");
	while (<ANN>) {
	    chomp;
	    push(@leng,$_);
	}
	if (@leng != @phy) {
	    print OUT "$file not the same length\n";
	    return 0;
	}
	my $notfound = 0;
	for (my $j = 0; $j < @leng; ++$j) {
	    my @arr = split(/\s/,$leng[$j]);
	    if (($phy[$j] =~/$arr[0]/) || ($phy[$j] =~ /$arr[1]/)){
		$phy[$j] = $phy[$j] ." " .$arr[2];
	    }else {
		#print "$phy[$j] $arr[0] $arr[1]\n";
		++$notfound;
	    }
	}
	if ($notfound > 0) {
	    print OUT "$file not the same order\n";
	    return 0;
	}else{
	   # print "addNei2:$phy[0]\n";
	    return 1;
	}
    } else {
	print OUT "file not found $file nei2\n";
	return 0;
    }
 }
