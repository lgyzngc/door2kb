#!/usr/local/bin/perl -w

#print commands to run large number of files

   my $linx = $ARGV[0]; #"ppc64linux";
   my $dir = $ARGV[1]; #"\/gpfs0\/home\/staff\/phd\/operonPreMay08";
   my $evalue = $ARGV[2]; #1e-20;
   my $genomeNum = $ARGV[3]; #675;
   my $NC_name = $ARGV[4];          
   my $NC_pair = $ARGV[5]; #$NC_name .".adjPair";
   my $NC_hom = $ARGV[6]; #$NC_name . ".ffn.hom";
   my $seqoutphy =$ARGV[7]; #"/scratch/phd/$NC_name.phyDist";
   my $scriptpath = $ARGV[8];
   my $neievalue = $ARGV[9]; # 1e-20;
   my $ncNum = $ARGV[10]; #1225;
   my $seqoutnei =$ARGV[11];
   my $NC_nextpair=$ARGV[12];
   my $seqoutphynext=$ARGV[13];
   my $seqoutneinext=$ARGV[14];
   ####################################################################
   
     
     my $out1 = "/scratch/phd/$NC_name.phyextract";
     my $cmdfile="$dir\/$NC_name.phy.sh";
     my $out2 = "/scratch/phd/$NC_name.neiextract";

     printjobfile();
    
sub printjobfile{
     
     open JOBFILE,">$cmdfile" or die "Can not open file: $cmdfile\n";

     print JOBFILE "#!/bin/bash\n";
     print JOBFILE "#\$ -l arch=$linx\n";
     print JOBFILE "#\$ -S /bin/bash\n";
     print JOBFILE "mkdir -p /scratch/phd\n";

     print JOBFILE "perl $scriptpath\/extract_phylo_wEcutoff_ffn.pl $dir/$NC_hom $evalue $genomeNum  $out1\n";
     print JOBFILE "perl $scriptpath\/cal-phylo-dist-for-gene-pair.pl $out1 $dir\/$NC_pair $dir/$seqoutphy\n";
     print JOBFILE "perl $scriptpath\/cal-phylo-dist-for-gene-pair.pl $out1 $dir\/$NC_nextpair $dir/$seqoutphynext\n";

     print JOBFILE "perl $scriptpath\/extract_phylo_wEcutoff_ffn_printGI.pl $dir/$NC_hom $neievalue $ncNum  $out2\n";
     print JOBFILE "perl $scriptpath\/calculatePairwiseHomologAdjacent_ffn.pl $out2 $dir\/$NC_pair $dir/$seqoutnei\n";
     print JOBFILE "perl $scriptpath\/calculatePairwiseHomologAdjacent_ffn.pl $out2 $dir\/$NC_nextpair $dir/$seqoutneinext\n";

     
     print JOBFILE "rm -rf \/scratch\/phd\/$NC_name*\n";
     close JOBFILE;
     system("chmod 755 $cmdfile");
     system("qsub $cmdfile");
     #sleep(0.3);
}
